create table IDEA_Comment (
	commentId LONG not null primary key,
	createDate DATE null,
	modifiedDate DATE null,
	companyId LONG,
	groupId LONG,
	IdeaRef LONG,
	commentRef LONG,
	commentText VARCHAR(75) null,
	userId LONG,
	userName VARCHAR(75) null
);

create table IDEA_Ideas (
	ideasId LONG not null primary key,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	groupId LONG,
	title VARCHAR(75) null,
	description STRING null,
	shortdescription VARCHAR(100) null,
	category VARCHAR(75) null,
	latitude DOUBLE,
	longitude DOUBLE,
	icon VARCHAR(75) null,
	published BOOLEAN,
	isVisibleOnMap BOOLEAN,
	rating INTEGER
);