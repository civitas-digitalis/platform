create table IDEA_Comment (
	commentId bigint not null primary key,
	createDate timestamp null,
	modifiedDate timestamp null,
	companyId bigint,
	groupId bigint,
	IdeaRef bigint,
	commentRef bigint,
	commentText varchar(75) null,
	userId bigint,
	userName varchar(75) null
);

create table IDEA_Ideas (
	ideasId bigint not null primary key,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate timestamp null,
	modifiedDate timestamp null,
	groupId bigint,
	title varchar(75) null,
	description text null,
	shortdescription varchar(100) null,
	category varchar(75) null,
	latitude double precision,
	longitude double precision,
	icon varchar(75) null,
	published bool,
	isVisibleOnMap bool,
	rating integer
);
