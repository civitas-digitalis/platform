create table IDEA_Comment (
	commentId bigint not null primary key,
	createDate datetime null,
	modifiedDate datetime null,
	companyId bigint,
	groupId bigint,
	IdeaRef bigint,
	commentRef bigint,
	commentText varchar(75) null,
	userId bigint,
	userName varchar(75) null
) engine InnoDB;

create table IDEA_Ideas (
	ideasId bigint not null primary key,
	companyId bigint,
	userId bigint,
	userName varchar(75) null,
	createDate datetime null,
	modifiedDate datetime null,
	groupId bigint,
	title varchar(75) null,
	description longtext null,
	shortdescription varchar(100) null,
	category varchar(75) null,
	latitude double,
	longitude double,
	icon varchar(75) null,
	published tinyint,
	isVisibleOnMap tinyint,
	rating integer
) engine InnoDB;
