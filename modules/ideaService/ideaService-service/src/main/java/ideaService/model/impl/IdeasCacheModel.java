/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import ideaService.model.Ideas;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Ideas in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Ideas
 * @generated
 */
@ProviderType
public class IdeasCacheModel implements CacheModel<Ideas>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IdeasCacheModel)) {
			return false;
		}

		IdeasCacheModel ideasCacheModel = (IdeasCacheModel)obj;

		if (ideasId == ideasCacheModel.ideasId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ideasId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{ideasId=");
		sb.append(ideasId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", title=");
		sb.append(title);
		sb.append(", description=");
		sb.append(description);
		sb.append(", shortdescription=");
		sb.append(shortdescription);
		sb.append(", category=");
		sb.append(category);
		sb.append(", latitude=");
		sb.append(latitude);
		sb.append(", longitude=");
		sb.append(longitude);
		sb.append(", icon=");
		sb.append(icon);
		sb.append(", published=");
		sb.append(published);
		sb.append(", isVisibleOnMap=");
		sb.append(isVisibleOnMap);
		sb.append(", rating=");
		sb.append(rating);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Ideas toEntityModel() {
		IdeasImpl ideasImpl = new IdeasImpl();

		ideasImpl.setIdeasId(ideasId);
		ideasImpl.setCompanyId(companyId);
		ideasImpl.setUserId(userId);

		if (userName == null) {
			ideasImpl.setUserName(StringPool.BLANK);
		}
		else {
			ideasImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			ideasImpl.setCreateDate(null);
		}
		else {
			ideasImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			ideasImpl.setModifiedDate(null);
		}
		else {
			ideasImpl.setModifiedDate(new Date(modifiedDate));
		}

		ideasImpl.setGroupId(groupId);

		if (title == null) {
			ideasImpl.setTitle(StringPool.BLANK);
		}
		else {
			ideasImpl.setTitle(title);
		}

		if (description == null) {
			ideasImpl.setDescription(StringPool.BLANK);
		}
		else {
			ideasImpl.setDescription(description);
		}

		if (shortdescription == null) {
			ideasImpl.setShortdescription(StringPool.BLANK);
		}
		else {
			ideasImpl.setShortdescription(shortdescription);
		}

		if (category == null) {
			ideasImpl.setCategory(StringPool.BLANK);
		}
		else {
			ideasImpl.setCategory(category);
		}

		ideasImpl.setLatitude(latitude);
		ideasImpl.setLongitude(longitude);

		if (icon == null) {
			ideasImpl.setIcon(StringPool.BLANK);
		}
		else {
			ideasImpl.setIcon(icon);
		}

		ideasImpl.setPublished(published);
		ideasImpl.setIsVisibleOnMap(isVisibleOnMap);
		ideasImpl.setRating(rating);

		ideasImpl.resetOriginalValues();

		return ideasImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ideasId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		groupId = objectInput.readLong();
		title = objectInput.readUTF();
		description = objectInput.readUTF();
		shortdescription = objectInput.readUTF();
		category = objectInput.readUTF();

		latitude = objectInput.readDouble();

		longitude = objectInput.readDouble();
		icon = objectInput.readUTF();

		published = objectInput.readBoolean();

		isVisibleOnMap = objectInput.readBoolean();

		rating = objectInput.readInt();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ideasId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(groupId);

		if (title == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(title);
		}

		if (description == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(description);
		}

		if (shortdescription == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(shortdescription);
		}

		if (category == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(category);
		}

		objectOutput.writeDouble(latitude);

		objectOutput.writeDouble(longitude);

		if (icon == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(icon);
		}

		objectOutput.writeBoolean(published);

		objectOutput.writeBoolean(isVisibleOnMap);

		objectOutput.writeInt(rating);
	}

	public long ideasId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long groupId;
	public String title;
	public String description;
	public String shortdescription;
	public String category;
	public double latitude;
	public double longitude;
	public String icon;
	public boolean published;
	public boolean isVisibleOnMap;
	public int rating;
}