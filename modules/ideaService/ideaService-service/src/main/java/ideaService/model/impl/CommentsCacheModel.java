/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import ideaService.model.Comments;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Comments in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Comments
 * @generated
 */
@ProviderType
public class CommentsCacheModel implements CacheModel<Comments>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CommentsCacheModel)) {
			return false;
		}

		CommentsCacheModel commentsCacheModel = (CommentsCacheModel)obj;

		if (commentId == commentsCacheModel.commentId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, commentId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{commentId=");
		sb.append(commentId);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", IdeaRef=");
		sb.append(IdeaRef);
		sb.append(", commentRef=");
		sb.append(commentRef);
		sb.append(", commentText=");
		sb.append(commentText);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Comments toEntityModel() {
		CommentsImpl commentsImpl = new CommentsImpl();

		commentsImpl.setCommentId(commentId);

		if (createDate == Long.MIN_VALUE) {
			commentsImpl.setCreateDate(null);
		}
		else {
			commentsImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			commentsImpl.setModifiedDate(null);
		}
		else {
			commentsImpl.setModifiedDate(new Date(modifiedDate));
		}

		commentsImpl.setCompanyId(companyId);
		commentsImpl.setGroupId(groupId);
		commentsImpl.setIdeaRef(IdeaRef);
		commentsImpl.setCommentRef(commentRef);

		if (commentText == null) {
			commentsImpl.setCommentText(StringPool.BLANK);
		}
		else {
			commentsImpl.setCommentText(commentText);
		}

		commentsImpl.setUserId(userId);

		if (userName == null) {
			commentsImpl.setUserName(StringPool.BLANK);
		}
		else {
			commentsImpl.setUserName(userName);
		}

		commentsImpl.resetOriginalValues();

		return commentsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		commentId = objectInput.readLong();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		companyId = objectInput.readLong();

		groupId = objectInput.readLong();

		IdeaRef = objectInput.readLong();

		commentRef = objectInput.readLong();
		commentText = objectInput.readUTF();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(commentId);
		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(IdeaRef);

		objectOutput.writeLong(commentRef);

		if (commentText == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(commentText);
		}

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(userName);
		}
	}

	public long commentId;
	public long createDate;
	public long modifiedDate;
	public long companyId;
	public long groupId;
	public long IdeaRef;
	public long commentRef;
	public String commentText;
	public long userId;
	public String userName;
}