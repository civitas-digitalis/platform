/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import ideaService.exception.NoSuchCommentsException;

import ideaService.model.Comments;

import ideaService.model.impl.CommentsImpl;
import ideaService.model.impl.CommentsModelImpl;

import ideaService.service.persistence.CommentsPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the comments service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CommentsPersistence
 * @see ideaService.service.persistence.CommentsUtil
 * @generated
 */
@ProviderType
public class CommentsPersistenceImpl extends BasePersistenceImpl<Comments>
	implements CommentsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CommentsUtil} to access the comments persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CommentsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, CommentsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public CommentsPersistenceImpl() {
		setModelClass(Comments.class);
	}

	/**
	 * Caches the comments in the entity cache if it is enabled.
	 *
	 * @param comments the comments
	 */
	@Override
	public void cacheResult(Comments comments) {
		entityCache.putResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsImpl.class, comments.getPrimaryKey(), comments);

		comments.resetOriginalValues();
	}

	/**
	 * Caches the commentses in the entity cache if it is enabled.
	 *
	 * @param commentses the commentses
	 */
	@Override
	public void cacheResult(List<Comments> commentses) {
		for (Comments comments : commentses) {
			if (entityCache.getResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
						CommentsImpl.class, comments.getPrimaryKey()) == null) {
				cacheResult(comments);
			}
			else {
				comments.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all commentses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(CommentsImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the comments.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Comments comments) {
		entityCache.removeResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsImpl.class, comments.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Comments> commentses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Comments comments : commentses) {
			entityCache.removeResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
				CommentsImpl.class, comments.getPrimaryKey());
		}
	}

	/**
	 * Creates a new comments with the primary key. Does not add the comments to the database.
	 *
	 * @param commentId the primary key for the new comments
	 * @return the new comments
	 */
	@Override
	public Comments create(long commentId) {
		Comments comments = new CommentsImpl();

		comments.setNew(true);
		comments.setPrimaryKey(commentId);

		comments.setCompanyId(companyProvider.getCompanyId());

		return comments;
	}

	/**
	 * Removes the comments with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param commentId the primary key of the comments
	 * @return the comments that was removed
	 * @throws NoSuchCommentsException if a comments with the primary key could not be found
	 */
	@Override
	public Comments remove(long commentId) throws NoSuchCommentsException {
		return remove((Serializable)commentId);
	}

	/**
	 * Removes the comments with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the comments
	 * @return the comments that was removed
	 * @throws NoSuchCommentsException if a comments with the primary key could not be found
	 */
	@Override
	public Comments remove(Serializable primaryKey)
		throws NoSuchCommentsException {
		Session session = null;

		try {
			session = openSession();

			Comments comments = (Comments)session.get(CommentsImpl.class,
					primaryKey);

			if (comments == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCommentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(comments);
		}
		catch (NoSuchCommentsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Comments removeImpl(Comments comments) {
		comments = toUnwrappedModel(comments);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(comments)) {
				comments = (Comments)session.get(CommentsImpl.class,
						comments.getPrimaryKeyObj());
			}

			if (comments != null) {
				session.delete(comments);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (comments != null) {
			clearCache(comments);
		}

		return comments;
	}

	@Override
	public Comments updateImpl(Comments comments) {
		comments = toUnwrappedModel(comments);

		boolean isNew = comments.isNew();

		CommentsModelImpl commentsModelImpl = (CommentsModelImpl)comments;

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (comments.getCreateDate() == null)) {
			if (serviceContext == null) {
				comments.setCreateDate(now);
			}
			else {
				comments.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!commentsModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				comments.setModifiedDate(now);
			}
			else {
				comments.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (comments.isNew()) {
				session.save(comments);

				comments.setNew(false);
			}
			else {
				comments = (Comments)session.merge(comments);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
			CommentsImpl.class, comments.getPrimaryKey(), comments, false);

		comments.resetOriginalValues();

		return comments;
	}

	protected Comments toUnwrappedModel(Comments comments) {
		if (comments instanceof CommentsImpl) {
			return comments;
		}

		CommentsImpl commentsImpl = new CommentsImpl();

		commentsImpl.setNew(comments.isNew());
		commentsImpl.setPrimaryKey(comments.getPrimaryKey());

		commentsImpl.setCommentId(comments.getCommentId());
		commentsImpl.setCreateDate(comments.getCreateDate());
		commentsImpl.setModifiedDate(comments.getModifiedDate());
		commentsImpl.setCompanyId(comments.getCompanyId());
		commentsImpl.setGroupId(comments.getGroupId());
		commentsImpl.setIdeaRef(comments.getIdeaRef());
		commentsImpl.setCommentRef(comments.getCommentRef());
		commentsImpl.setCommentText(comments.getCommentText());
		commentsImpl.setUserId(comments.getUserId());
		commentsImpl.setUserName(comments.getUserName());

		return commentsImpl;
	}

	/**
	 * Returns the comments with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the comments
	 * @return the comments
	 * @throws NoSuchCommentsException if a comments with the primary key could not be found
	 */
	@Override
	public Comments findByPrimaryKey(Serializable primaryKey)
		throws NoSuchCommentsException {
		Comments comments = fetchByPrimaryKey(primaryKey);

		if (comments == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchCommentsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return comments;
	}

	/**
	 * Returns the comments with the primary key or throws a {@link NoSuchCommentsException} if it could not be found.
	 *
	 * @param commentId the primary key of the comments
	 * @return the comments
	 * @throws NoSuchCommentsException if a comments with the primary key could not be found
	 */
	@Override
	public Comments findByPrimaryKey(long commentId)
		throws NoSuchCommentsException {
		return findByPrimaryKey((Serializable)commentId);
	}

	/**
	 * Returns the comments with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the comments
	 * @return the comments, or <code>null</code> if a comments with the primary key could not be found
	 */
	@Override
	public Comments fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
				CommentsImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Comments comments = (Comments)serializable;

		if (comments == null) {
			Session session = null;

			try {
				session = openSession();

				comments = (Comments)session.get(CommentsImpl.class, primaryKey);

				if (comments != null) {
					cacheResult(comments);
				}
				else {
					entityCache.putResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
						CommentsImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
					CommentsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return comments;
	}

	/**
	 * Returns the comments with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param commentId the primary key of the comments
	 * @return the comments, or <code>null</code> if a comments with the primary key could not be found
	 */
	@Override
	public Comments fetchByPrimaryKey(long commentId) {
		return fetchByPrimaryKey((Serializable)commentId);
	}

	@Override
	public Map<Serializable, Comments> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Comments> map = new HashMap<Serializable, Comments>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Comments comments = fetchByPrimaryKey(primaryKey);

			if (comments != null) {
				map.put(primaryKey, comments);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
					CommentsImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Comments)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_COMMENTS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Comments comments : (List<Comments>)q.list()) {
				map.put(comments.getPrimaryKeyObj(), comments);

				cacheResult(comments);

				uncachedPrimaryKeys.remove(comments.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(CommentsModelImpl.ENTITY_CACHE_ENABLED,
					CommentsImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the commentses.
	 *
	 * @return the commentses
	 */
	@Override
	public List<Comments> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the commentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CommentsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @return the range of commentses
	 */
	@Override
	public List<Comments> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the commentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CommentsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of commentses
	 */
	@Override
	public List<Comments> findAll(int start, int end,
		OrderByComparator<Comments> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the commentses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CommentsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of commentses
	 * @param end the upper bound of the range of commentses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of commentses
	 */
	@Override
	public List<Comments> findAll(int start, int end,
		OrderByComparator<Comments> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Comments> list = null;

		if (retrieveFromCache) {
			list = (List<Comments>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_COMMENTS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_COMMENTS;

				if (pagination) {
					sql = sql.concat(CommentsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Comments>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Comments>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the commentses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Comments comments : findAll()) {
			remove(comments);
		}
	}

	/**
	 * Returns the number of commentses.
	 *
	 * @return the number of commentses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_COMMENTS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return CommentsModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the comments persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(CommentsImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_COMMENTS = "SELECT comments FROM Comments comments";
	private static final String _SQL_SELECT_COMMENTS_WHERE_PKS_IN = "SELECT comments FROM Comments comments WHERE commentId IN (";
	private static final String _SQL_COUNT_COMMENTS = "SELECT COUNT(comments) FROM Comments comments";
	private static final String _ORDER_BY_ENTITY_ALIAS = "comments.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Comments exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(CommentsPersistenceImpl.class);
}