/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.http;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import ideaService.service.IdeasServiceUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link IdeasServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link ideaService.model.IdeasSoap}.
 * If the method in the service utility returns a
 * {@link ideaService.model.Ideas}, that is translated to a
 * {@link ideaService.model.IdeasSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IdeasServiceHttp
 * @see ideaService.model.IdeasSoap
 * @see IdeasServiceUtil
 * @generated
 */
@ProviderType
public class IdeasServiceSoap {
	/**
	* finds and returns all ideas.
	*/
	public static java.lang.String getAllIdeas() throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.getAllIdeas();

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getIdeaById(long id)
		throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.getIdeaById(id);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* Finds all ideas with Category category.
	* This is a remote function. Not to be used locally!
	*
	* @param cat the desired cat
	* @return a list with all ideas that have type equal to input param
	*/
	public static java.lang.String getIdeasByCategory(java.lang.String category)
		throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.getIdeasByCategory(category);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* @param rating
	* @return
	*/
	public static java.lang.String getIdeasByRating(int rating)
		throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.getIdeasByRating(rating);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* @param ratingRangeStart
	* @param ratingRangeEnd
	* @return all ideas with the given range.
	*/
	public static java.lang.String getIdeasByRatingRange(int ratingRangeStart,
		int ratingRangeEnd) throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.getIdeasByRatingRange(ratingRangeStart,
					ratingRangeEnd);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String searchIdeasByField(
		java.lang.String fieldName, java.lang.String query, long[] groupIds)
		throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.searchIdeasByField(fieldName,
					query, groupIds);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String seachIdeasByFieldArray(
		java.lang.String[] fieldNames, java.lang.String query, long[] groupIds)
		throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.seachIdeasByFieldArray(fieldNames,
					query, groupIds);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String searchIdeasByFieldArrayQueryArray(
		java.lang.String[] fieldNames, java.lang.String[] queries,
		long[] groupIds) throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.searchIdeasByFieldArrayQueryArray(fieldNames,
					queries, groupIds);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getIdeasByIsPublished(boolean published)
		throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.getIdeasByIsPublished(published);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String getIdeasByIsVisibleOnMap(boolean visible)
		throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.getIdeasByIsVisibleOnMap(visible);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	/**
	* @param pk the primaryKey
	* @param rating the rating to set
	* @return reponse json
	*/
	public static java.lang.String updateIdeasRating(long pk, int rating)
		throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.updateIdeasRating(pk,
					rating);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String insertNewIdea(java.lang.String title,
		java.lang.String category, long userId, java.lang.String icon,
		java.lang.String shortDescription, java.lang.String description,
		double latitude, double longitude, boolean published,
		boolean showOnMap, long groupId, int rating) throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.insertNewIdea(title,
					category, userId, icon, shortDescription, description,
					latitude, longitude, published, showOnMap, groupId, rating);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static java.lang.String deleteIdea(java.lang.String Id)
		throws RemoteException {
		try {
			java.lang.String returnValue = IdeasServiceUtil.deleteIdea(Id);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(IdeasServiceSoap.class);
}