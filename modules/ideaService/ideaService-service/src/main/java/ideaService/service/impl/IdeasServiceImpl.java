/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import aQute.bnd.annotation.ProviderType;
import ideaService.model.Ideas;
import ideaService.service.IdeasLocalServiceUtil;
import ideaService.service.base.IdeasServiceBaseImpl;
import ideaService.service.persistence.IdeasUtil;

/**
 * The implementation of the ideas remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link ideaService.service.IdeasService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have
 * security checks based on the propagated JAAS credentials because this service
 * can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IdeasServiceBaseImpl
 * @see ideaService.service.IdeasServiceUtil
 */
@ProviderType
public class IdeasServiceImpl extends IdeasServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * ideaService.service.IdeasServiceUtil} to access the ideas remote service.
	 */
	/**
	 * finds and returns all ideas.
	 */
	@JSONWebService(method = "GET")
	public String getAllIdeas() {
		return JSONFactoryUtil.looseSerialize(IdeasLocalServiceUtil.getAllIdeas());
	}

	@JSONWebService(method = "GET")
	public String getIdeaById(long id){
		return JSONFactoryUtil.looseSerialize(IdeasUtil.fetchByPrimaryKey(id));
	}

	/**
	 * Finds all ideas with Category category.
	 * This is a remote function. Not to be used locally!
	 * @param cat the desired cat
	 * @return a list with all ideas that have type equal to input param
	 */
	@JSONWebService(method = "GET")
	public String getIdeasByCategory(String category){
		return JSONFactoryUtil.looseSerialize(IdeasLocalServiceUtil.getIdeasByCategory(category));
	}

	/**
	 *
	 * @param rating
	 * @return
	 */
	@JSONWebService(method = "GET")
	public String getIdeasByRating(int rating){
		return JSONFactoryUtil.looseSerialize(IdeasLocalServiceUtil.getIdeasByRating(rating));
	}
	/**
	 *
	 * @param ratingRangeStart
	 * @param ratingRangeEnd
	 * @return all ideas with the given range.
	 */
	@JSONWebService(method = "GET")
	public String getIdeasByRatingRange(int ratingRangeStart,  int ratingRangeEnd){
		return JSONFactoryUtil.looseSerialize(IdeasLocalServiceUtil.getIdeasByRatingRange(ratingRangeStart,ratingRangeEnd));
	}

	@JSONWebService(method = "GET")
	public String searchIdeasByField(String fieldName, String query, long [] groupIds){
		return JSONFactoryUtil.looseSerialize(IdeasLocalServiceUtil.searchIdeasByField(fieldName, query, groupIds));
	}

	@JSONWebService(method = "GET")
	public String seachIdeasByFieldArray(String [] fieldNames, String query, long [] groupIds){
		return JSONFactoryUtil.looseSerialize(IdeasLocalServiceUtil.seachIdeasByFieldArray(fieldNames, query, groupIds));
	}
	@JSONWebService(method = "GET")
	public String searchIdeasByFieldArrayQueryArray(String [] fieldNames, String [] queries, long [] groupIds){
		return JSONFactoryUtil.looseSerialize(IdeasLocalServiceUtil.searchIdeasByFieldArrayQueryArray(fieldNames, queries, groupIds));
	}
	@JSONWebService(method = "GET")
	public String getIdeasByIsPublished(boolean published){
		return JSONFactoryUtil.looseSerialize(IdeasLocalServiceUtil.getIdeasByIsPublished(published));
	}
	@JSONWebService(method = "GET")
	public String getIdeasByIsVisibleOnMap(boolean visible){
		return JSONFactoryUtil.looseSerialize(IdeasLocalServiceUtil.getIdeasByIsVisibleOnMap(visible));
	}


	/**
	 * @param pk the primaryKey
	 * @param rating the rating to set
	 * @return reponse json
	 */
	@JSONWebService(method = "PUT")
	public String updateIdeasRating(long pk, int rating){
		IdeasLocalServiceUtil.setRating(pk,rating);
		JSONObject response = JSONFactoryUtil.createJSONObject();
		response.put("created", true);
		return JSONFactoryUtil.looseSerialize(response);
	}

	@JSONWebService(method = "POST")
	public String insertNewIdea(String title,String category,long userId, String icon,String shortDescription,
			String description,double latitude, double longitude, boolean published, boolean showOnMap, long groupId, int rating) {
		Ideas idea = IdeasLocalServiceUtil.creadeIdeasWithAutomatedDbId(title, userId, groupId, category,
				icon, shortDescription, description, latitude, longitude, published, showOnMap, rating);
		if(!idea.equals(null)){
			idea.persist();
			JSONObject response = JSONFactoryUtil.createJSONObject();
			response.put("created", true);
			response.put("createdId", idea.getPrimaryKey());
			return JSONFactoryUtil.looseSerialize(response);
		} else {
			JSONObject response = JSONFactoryUtil.createJSONObject();
			response.put("created", false);
			response.put("errorMsg" , "please fill out all input fields");
			return JSONFactoryUtil.looseSerialize(response);
		}
	}

	@JSONWebService(method = "DELETE")
	public String deleteIdea(String Id){
		try {
			IdeasLocalServiceUtil.deleteIdeas(Long.parseLong(Id));
			JSONObject response = JSONFactoryUtil.createJSONObject();
			response.put("deleted", true);
			return JSONFactoryUtil.looseSerialize(response);
		} catch (NumberFormatException | PortalException e) {
			JSONObject response = JSONFactoryUtil.createJSONObject();
			response.put("deleted", false);
			return JSONFactoryUtil.looseSerialize(response);
		}
	}
}