/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import ideaService.exception.NoSuchIdeasException;

import ideaService.model.Ideas;

import ideaService.model.impl.IdeasImpl;
import ideaService.model.impl.IdeasModelImpl;

import ideaService.service.persistence.IdeasPersistence;

import java.io.Serializable;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the ideas service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IdeasPersistence
 * @see ideaService.service.persistence.IdeasUtil
 * @generated
 */
@ProviderType
public class IdeasPersistenceImpl extends BasePersistenceImpl<Ideas>
	implements IdeasPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link IdeasUtil} to access the ideas persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = IdeasImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(IdeasModelImpl.ENTITY_CACHE_ENABLED,
			IdeasModelImpl.FINDER_CACHE_ENABLED, IdeasImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(IdeasModelImpl.ENTITY_CACHE_ENABLED,
			IdeasModelImpl.FINDER_CACHE_ENABLED, IdeasImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(IdeasModelImpl.ENTITY_CACHE_ENABLED,
			IdeasModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public IdeasPersistenceImpl() {
		setModelClass(Ideas.class);
	}

	/**
	 * Caches the ideas in the entity cache if it is enabled.
	 *
	 * @param ideas the ideas
	 */
	@Override
	public void cacheResult(Ideas ideas) {
		entityCache.putResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
			IdeasImpl.class, ideas.getPrimaryKey(), ideas);

		ideas.resetOriginalValues();
	}

	/**
	 * Caches the ideases in the entity cache if it is enabled.
	 *
	 * @param ideases the ideases
	 */
	@Override
	public void cacheResult(List<Ideas> ideases) {
		for (Ideas ideas : ideases) {
			if (entityCache.getResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
						IdeasImpl.class, ideas.getPrimaryKey()) == null) {
				cacheResult(ideas);
			}
			else {
				ideas.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ideases.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(IdeasImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ideas.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Ideas ideas) {
		entityCache.removeResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
			IdeasImpl.class, ideas.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Ideas> ideases) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Ideas ideas : ideases) {
			entityCache.removeResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
				IdeasImpl.class, ideas.getPrimaryKey());
		}
	}

	/**
	 * Creates a new ideas with the primary key. Does not add the ideas to the database.
	 *
	 * @param ideasId the primary key for the new ideas
	 * @return the new ideas
	 */
	@Override
	public Ideas create(long ideasId) {
		Ideas ideas = new IdeasImpl();

		ideas.setNew(true);
		ideas.setPrimaryKey(ideasId);

		ideas.setCompanyId(companyProvider.getCompanyId());

		return ideas;
	}

	/**
	 * Removes the ideas with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ideasId the primary key of the ideas
	 * @return the ideas that was removed
	 * @throws NoSuchIdeasException if a ideas with the primary key could not be found
	 */
	@Override
	public Ideas remove(long ideasId) throws NoSuchIdeasException {
		return remove((Serializable)ideasId);
	}

	/**
	 * Removes the ideas with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ideas
	 * @return the ideas that was removed
	 * @throws NoSuchIdeasException if a ideas with the primary key could not be found
	 */
	@Override
	public Ideas remove(Serializable primaryKey) throws NoSuchIdeasException {
		Session session = null;

		try {
			session = openSession();

			Ideas ideas = (Ideas)session.get(IdeasImpl.class, primaryKey);

			if (ideas == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchIdeasException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ideas);
		}
		catch (NoSuchIdeasException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Ideas removeImpl(Ideas ideas) {
		ideas = toUnwrappedModel(ideas);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ideas)) {
				ideas = (Ideas)session.get(IdeasImpl.class,
						ideas.getPrimaryKeyObj());
			}

			if (ideas != null) {
				session.delete(ideas);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ideas != null) {
			clearCache(ideas);
		}

		return ideas;
	}

	@Override
	public Ideas updateImpl(Ideas ideas) {
		ideas = toUnwrappedModel(ideas);

		boolean isNew = ideas.isNew();

		IdeasModelImpl ideasModelImpl = (IdeasModelImpl)ideas;

		ServiceContext serviceContext = ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (ideas.getCreateDate() == null)) {
			if (serviceContext == null) {
				ideas.setCreateDate(now);
			}
			else {
				ideas.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!ideasModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				ideas.setModifiedDate(now);
			}
			else {
				ideas.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (ideas.isNew()) {
				session.save(ideas);

				ideas.setNew(false);
			}
			else {
				ideas = (Ideas)session.merge(ideas);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
			IdeasImpl.class, ideas.getPrimaryKey(), ideas, false);

		ideas.resetOriginalValues();

		return ideas;
	}

	protected Ideas toUnwrappedModel(Ideas ideas) {
		if (ideas instanceof IdeasImpl) {
			return ideas;
		}

		IdeasImpl ideasImpl = new IdeasImpl();

		ideasImpl.setNew(ideas.isNew());
		ideasImpl.setPrimaryKey(ideas.getPrimaryKey());

		ideasImpl.setIdeasId(ideas.getIdeasId());
		ideasImpl.setCompanyId(ideas.getCompanyId());
		ideasImpl.setUserId(ideas.getUserId());
		ideasImpl.setUserName(ideas.getUserName());
		ideasImpl.setCreateDate(ideas.getCreateDate());
		ideasImpl.setModifiedDate(ideas.getModifiedDate());
		ideasImpl.setGroupId(ideas.getGroupId());
		ideasImpl.setTitle(ideas.getTitle());
		ideasImpl.setDescription(ideas.getDescription());
		ideasImpl.setShortdescription(ideas.getShortdescription());
		ideasImpl.setCategory(ideas.getCategory());
		ideasImpl.setLatitude(ideas.getLatitude());
		ideasImpl.setLongitude(ideas.getLongitude());
		ideasImpl.setIcon(ideas.getIcon());
		ideasImpl.setPublished(ideas.isPublished());
		ideasImpl.setIsVisibleOnMap(ideas.isIsVisibleOnMap());
		ideasImpl.setRating(ideas.getRating());

		return ideasImpl;
	}

	/**
	 * Returns the ideas with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ideas
	 * @return the ideas
	 * @throws NoSuchIdeasException if a ideas with the primary key could not be found
	 */
	@Override
	public Ideas findByPrimaryKey(Serializable primaryKey)
		throws NoSuchIdeasException {
		Ideas ideas = fetchByPrimaryKey(primaryKey);

		if (ideas == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchIdeasException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ideas;
	}

	/**
	 * Returns the ideas with the primary key or throws a {@link NoSuchIdeasException} if it could not be found.
	 *
	 * @param ideasId the primary key of the ideas
	 * @return the ideas
	 * @throws NoSuchIdeasException if a ideas with the primary key could not be found
	 */
	@Override
	public Ideas findByPrimaryKey(long ideasId) throws NoSuchIdeasException {
		return findByPrimaryKey((Serializable)ideasId);
	}

	/**
	 * Returns the ideas with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ideas
	 * @return the ideas, or <code>null</code> if a ideas with the primary key could not be found
	 */
	@Override
	public Ideas fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
				IdeasImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Ideas ideas = (Ideas)serializable;

		if (ideas == null) {
			Session session = null;

			try {
				session = openSession();

				ideas = (Ideas)session.get(IdeasImpl.class, primaryKey);

				if (ideas != null) {
					cacheResult(ideas);
				}
				else {
					entityCache.putResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
						IdeasImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
					IdeasImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ideas;
	}

	/**
	 * Returns the ideas with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ideasId the primary key of the ideas
	 * @return the ideas, or <code>null</code> if a ideas with the primary key could not be found
	 */
	@Override
	public Ideas fetchByPrimaryKey(long ideasId) {
		return fetchByPrimaryKey((Serializable)ideasId);
	}

	@Override
	public Map<Serializable, Ideas> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Ideas> map = new HashMap<Serializable, Ideas>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Ideas ideas = fetchByPrimaryKey(primaryKey);

			if (ideas != null) {
				map.put(primaryKey, ideas);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
					IdeasImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Ideas)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_IDEAS_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Ideas ideas : (List<Ideas>)q.list()) {
				map.put(ideas.getPrimaryKeyObj(), ideas);

				cacheResult(ideas);

				uncachedPrimaryKeys.remove(ideas.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(IdeasModelImpl.ENTITY_CACHE_ENABLED,
					IdeasImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the ideases.
	 *
	 * @return the ideases
	 */
	@Override
	public List<Ideas> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ideases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ideases
	 * @param end the upper bound of the range of ideases (not inclusive)
	 * @return the range of ideases
	 */
	@Override
	public List<Ideas> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ideases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ideases
	 * @param end the upper bound of the range of ideases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ideases
	 */
	@Override
	public List<Ideas> findAll(int start, int end,
		OrderByComparator<Ideas> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ideases.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ideases
	 * @param end the upper bound of the range of ideases (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of ideases
	 */
	@Override
	public List<Ideas> findAll(int start, int end,
		OrderByComparator<Ideas> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Ideas> list = null;

		if (retrieveFromCache) {
			list = (List<Ideas>)finderCache.getResult(finderPath, finderArgs,
					this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_IDEAS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_IDEAS;

				if (pagination) {
					sql = sql.concat(IdeasModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Ideas>)QueryUtil.list(q, getDialect(), start,
							end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Ideas>)QueryUtil.list(q, getDialect(), start,
							end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ideases from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Ideas ideas : findAll()) {
			remove(ideas);
		}
	}

	/**
	 * Returns the number of ideases.
	 *
	 * @return the number of ideases
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_IDEAS);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return IdeasModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the ideas persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(IdeasImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;
	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_IDEAS = "SELECT ideas FROM Ideas ideas";
	private static final String _SQL_SELECT_IDEAS_WHERE_PKS_IN = "SELECT ideas FROM Ideas ideas WHERE ideasId IN (";
	private static final String _SQL_COUNT_IDEAS = "SELECT COUNT(ideas) FROM Ideas ideas";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ideas.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Ideas exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(IdeasPersistenceImpl.class);
}