/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.document.library.kernel.service.DLFileEntryLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.ClassedModel;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Document;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.Indexer;
import com.liferay.portal.kernel.search.IndexerRegistryUtil;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchContextFactory;
import com.liferay.portal.kernel.search.SearchEngineHelperUtil;
import com.liferay.portal.kernel.search.SearchException;
import com.liferay.portal.kernel.search.generic.BooleanQueryImpl;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;
import com.liferay.portal.kernel.social.SocialActivityManagerUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.social.kernel.model.SocialActivityConstants;
import com.liferay.social.kernel.service.SocialActivityInterpreterLocalServiceUtil;
import com.liferay.social.kernel.service.SocialActivityLocalServiceUtil;

import aQute.bnd.annotation.ProviderType;
import ideaService.exception.NoSuchIdeasException;
import ideaService.model.Ideas;
import ideaService.service.IdeasLocalServiceUtil;
import ideaService.service.base.IdeasLocalServiceBaseImpl;
import ideaService.service.persistence.IdeasUtil;

/**
 * The implementation of the ideas local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link ideaService.service.IdeasLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IdeasLocalServiceBaseImpl
 * @see ideaService.service.IdeasLocalServiceUtil
 */
@ProviderType
public class IdeasLocalServiceImpl extends IdeasLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link ideaService.service.IdeasLocalServiceUtil} to access the ideas local service.
	 */

	private final String ENTRY_CLASS_NAME = "entryClassName";
	private final String ENTRY_CLASS_PK = "entryClassPK";

	/**
	 * returns all ideas as a list.
	 */
	public List<Ideas> getAllIdeas(){
		return IdeasUtil.findAll();
	}

	/**
	 * sets the rating of the idea with primaryKey pk
	 * @param pk the primary key
	 * @param rating the rating to set
	 */
	public void setRating(long pk, int rating){
		try {
			Ideas idea = IdeasUtil.findByPrimaryKey(pk);
			idea.setRating(rating);
			IdeasUtil.update(idea);
		} catch (NoSuchIdeasException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates a new Idea using the supplied parameters.
	 * created date, modified date and user name are set automatically.
	 */
	public Ideas creadeIdeasWithAutomatedDbId(String title,long userId,long groupId,String type, String icon,String shortDescription ,String description,double latitude, double longitude, boolean published, boolean showOnMap, int rating){
		int nextDbId = (int)CounterLocalServiceUtil.increment(Ideas.class.getName());
		Ideas nextIdea = IdeasLocalServiceUtil.createIdeas(nextDbId);
		nextIdea.setGroupId(groupId);
		nextIdea.setTitle(title);
		nextIdea.setCategory(type);
		nextIdea.setIcon(icon);
		nextIdea.setDescription(description);
		nextIdea.setShortdescription(shortDescription);
		nextIdea.setUserId(userId);
		nextIdea.setLatitude(latitude);
		nextIdea.setLongitude(longitude);
		nextIdea.setPublished(published);
		nextIdea.setIsVisibleOnMap(showOnMap);

		try {
			User user = UserLocalServiceUtil.getUser(userId);
			nextIdea.setUserName(user.getScreenName());
		} catch (PortalException e) {
			e.printStackTrace();
		}

		return nextIdea;
	}
	/**
	 * persists the new Idea and performs some checks e.g. if the entry is a duplicate it won't be inserted.
	 * @param idea
	 */
	public void persistIdeasAndPerformTypeChecks(Ideas idea){
		try {
			AssetEntry entry = assetEntryLocalService.updateEntry(idea.getUserId(), idea.getGroupId(), idea.getCreateDate(), idea.getModifiedDate(),
					Ideas.class.getName(), idea.getPrimaryKey(),PortalUUIDUtil.generate(),0L,null, null, true, true,
					null, null, new Date(), null,ContentTypes.TEXT_HTML, idea.getTitle(), idea.getDescription(), null, null,null, 0,0, null);
		Indexer<Ideas> indexer = IndexerRegistryUtil.nullSafeGetIndexer(Ideas.class);
		JSONObject extraDataJSONObject = JSONFactoryUtil.createJSONObject();
		extraDataJSONObject.put("title", idea.getTitle());
		SocialActivityManagerUtil.addActivity(entry.getUserId(), entry, SocialActivityConstants.TYPE_ADD_COMMENT, extraDataJSONObject.toString(), entry.getUserId());
		indexer.reindex(idea);
	} catch (PortalException e) {
			e.printStackTrace();
	}
		idea.persist();

//Search example
//		SearchContext s = new SearchContext();
//		s.setCompanyId(PortalUtil.getDefaultCompanyId());
//		long [] gid = new long[1];
//		gid[0] = idea.getGroupId();
//		s.setGroupIds(gid);
//
//		BooleanQueryImpl booleanQueryImpl=new BooleanQueryImpl();
//		booleanQueryImpl.addRequiredTerm(Field.ENTRY_CLASS_NAME, Ideas.class.getName());
//		booleanQueryImpl.addRequiredTerm(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
//		booleanQueryImpl.addRequiredTerm(Field.DESCRIPTION,"A");
//		try {
//			Hits results = null;
//			results=SearchEngineHelperUtil.getSearchEngine(SearchEngineHelperUtil.getDefaultSearchEngineId()).getIndexSearcher().search(s, booleanQueryImpl);
//			Document[] docs = results.getDocs();
//			for(int i=0; i<docs.length; i++){
//				System.out.println(i + " : " + docs[i]);
//			}
//			} catch (SearchException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			}

	}

	/**
	 * Searches the DB forthe given idea with a field that contains the query.
	 * Only supports search within text fields.
	 * @param fieldName
	 * @param query
	 * @param groupIds
	 * @return a list of ideas with each containing the query.
	 */
	public List<Ideas> searchIdeasByField(String fieldName, String query, long [] groupIds){
		SearchContext searchContext  = new SearchContext();
		searchContext.setCompanyId(PortalUtil.getDefaultCompanyId());
		searchContext.setGroupIds(groupIds);
		BooleanQueryImpl booleanQueryImpl = new BooleanQueryImpl();
		booleanQueryImpl.addRequiredTerm(fieldName, query);
		return searchIdeas(searchContext ,booleanQueryImpl);
	}

	/**
	 * Searches the DB for the given idea with a field that contains the query.
	 * Only returns a result if the query is found in all given fields.
	 * Only supports search within text fields.
	 * @param fieldNames
	 * @param query
	 * @param groupIds
	 * @return a list of ideas with each containing the query.
	 */
	public List<Ideas> seachIdeasByFieldArray(String [] fieldNames, String query, long [] groupIds){
		SearchContext searchContext  = new SearchContext();
		searchContext.setCompanyId(PortalUtil.getDefaultCompanyId());
		searchContext.setGroupIds(groupIds);
		BooleanQueryImpl booleanQueryImpl = new BooleanQueryImpl();
		for(int i=0; i<fieldNames.length; i++){
		booleanQueryImpl.addRequiredTerm(fieldNames[i], query);
		}
		return searchIdeas(searchContext ,booleanQueryImpl);
	}

	/**
	 * Searches the DB for any field that contains the query.
	 * Only supports search within text fields.
	 * @param fieldNames
	 * @param query
	 * @param groupIds
	 * @return a list of ideas with each containing the query.
	 */
	public List<Ideas> searchIdeasByFieldArrayQueryArray(String [] fieldNames, String [] queries, long [] groupIds){
		SearchContext searchContext = new SearchContext();
		searchContext.setCompanyId(PortalUtil.getDefaultCompanyId());
		searchContext.setGroupIds(groupIds);
		BooleanQueryImpl booleanQueryImpl = new BooleanQueryImpl();
		for(int i=0; i<fieldNames.length; i++){
		booleanQueryImpl.addRequiredTerm(fieldNames[i], queries[i]);
		}
		return searchIdeas(searchContext ,booleanQueryImpl);
	}

	/**
	 *
	 * @param s searchContext
	 * @param booleanQueryImpl bql
	 * @return List with Ideas according to s
	 */
	private List<Ideas> searchIdeas(SearchContext s, BooleanQueryImpl booleanQueryImpl){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		Hits searchResults = null;
		try {
			searchResults=SearchEngineHelperUtil.getSearchEngine(SearchEngineHelperUtil.getDefaultSearchEngineId()).getIndexSearcher().search(s, booleanQueryImpl);
		} catch (SearchException e) {
			e.printStackTrace();
		}
		Document[] docs = searchResults.getDocs();
		for(int i=0; i<docs.length; i++){
			if(docs[i].get(ENTRY_CLASS_NAME).equals(Ideas.class.getName())){
				try {
					result.add(IdeasUtil.findByPrimaryKey(Long.parseLong(docs[i].get(ENTRY_CLASS_PK ))));
				} catch (NoSuchIdeasException | NumberFormatException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	/**
	 * Finds all ideas with Category cat.
	 * @param cat the desired cat
	 * @return a list with all ideas that have type equal to input param
	 */
	public List<Ideas> getIdeasByCategory(String cat){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i: IdeasUtil.findAll()){
			if(i.getCategory().equals(cat)){
				result.add(i);
			}
		}
		return result;
	}

	/**
	 * Find all ideas with Rating rating.
	 * @param rating the exact rating
	 * @return a list with all ideas that have rating equal to param
	 */
	public List<Ideas> getIdeasByRating(int rating){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i: IdeasUtil.findAll()){
			if(i.getRating() == rating){
				result.add(i);
			}
		}
		return result;
	}

	/**
	 *
	 * @param ratingRangeStart the start of the rating range.
	 * @param ratingRangeEnd the end of the rating range.
	 * @return a list with all ideas that are within the given range.
	 */
	public List<Ideas> getIdeasByRatingRange(int ratingRangeStart, int ratingRangeEnd){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i: IdeasUtil.findAll()){
			if(i.getRating() >= ratingRangeStart && i.getRating() <= ratingRangeEnd){
				result.add(i);
			}
		}
		return result;
	}

	public List<Ideas> getIdeasByIsPublished(boolean published){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i: IdeasUtil.findAll()){
			if(i.getPublished() == published){
				result.add(i);
			}
		}
		return result;
	}

	public List<Ideas> getIdeasByIsVisibleOnMap(boolean visible){
		ArrayList<Ideas> result = new ArrayList<Ideas>();
		for(Ideas i: IdeasUtil.findAll()){
			if(i.getPublished() ==  visible){
				result.add(i);
			}
		}
		return result;
	}

	/**
	 * Deletes the idea with param id.
	 * @param id
	 */
	public void deleteIdea(long id) throws NoSuchIdeasException{
		try {
			assetEntryLocalService.deleteEntry(id);
			Indexer<Ideas> indexer = IndexerRegistryUtil.nullSafeGetIndexer(Ideas.class);
			indexer.delete(IdeasLocalServiceUtil.getIdeas(id));
		} catch (PortalException e) {
			e.printStackTrace();
		}
			IdeasUtil.remove(id);
		}
}



