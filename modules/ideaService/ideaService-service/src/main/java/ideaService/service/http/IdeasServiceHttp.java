/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.http;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import ideaService.service.IdeasServiceUtil;

/**
 * Provides the HTTP utility for the
 * {@link IdeasServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * {@link HttpPrincipal} parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see IdeasServiceSoap
 * @see HttpPrincipal
 * @see IdeasServiceUtil
 * @generated
 */
@ProviderType
public class IdeasServiceHttp {
	public static java.lang.String getAllIdeas(HttpPrincipal httpPrincipal) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getAllIdeas", _getAllIdeasParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(methodKey);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getIdeaById(HttpPrincipal httpPrincipal,
		long id) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeaById", _getIdeaByIdParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey, id);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getIdeasByCategory(
		HttpPrincipal httpPrincipal, java.lang.String category) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeasByCategory", _getIdeasByCategoryParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey, category);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getIdeasByRating(
		HttpPrincipal httpPrincipal, int rating) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeasByRating", _getIdeasByRatingParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(methodKey, rating);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getIdeasByRatingRange(
		HttpPrincipal httpPrincipal, int ratingRangeStart, int ratingRangeEnd) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeasByRatingRange",
					_getIdeasByRatingRangeParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					ratingRangeStart, ratingRangeEnd);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String searchIdeasByField(
		HttpPrincipal httpPrincipal, java.lang.String fieldName,
		java.lang.String query, long[] groupIds) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"searchIdeasByField", _searchIdeasByFieldParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					fieldName, query, groupIds);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String seachIdeasByFieldArray(
		HttpPrincipal httpPrincipal, java.lang.String[] fieldNames,
		java.lang.String query, long[] groupIds) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"seachIdeasByFieldArray",
					_seachIdeasByFieldArrayParameterTypes6);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					fieldNames, query, groupIds);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String searchIdeasByFieldArrayQueryArray(
		HttpPrincipal httpPrincipal, java.lang.String[] fieldNames,
		java.lang.String[] queries, long[] groupIds) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"searchIdeasByFieldArrayQueryArray",
					_searchIdeasByFieldArrayQueryArrayParameterTypes7);

			MethodHandler methodHandler = new MethodHandler(methodKey,
					fieldNames, queries, groupIds);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getIdeasByIsPublished(
		HttpPrincipal httpPrincipal, boolean published) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeasByIsPublished",
					_getIdeasByIsPublishedParameterTypes8);

			MethodHandler methodHandler = new MethodHandler(methodKey, published);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String getIdeasByIsVisibleOnMap(
		HttpPrincipal httpPrincipal, boolean visible) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"getIdeasByIsVisibleOnMap",
					_getIdeasByIsVisibleOnMapParameterTypes9);

			MethodHandler methodHandler = new MethodHandler(methodKey, visible);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String updateIdeasRating(
		HttpPrincipal httpPrincipal, long pk, int rating) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"updateIdeasRating", _updateIdeasRatingParameterTypes10);

			MethodHandler methodHandler = new MethodHandler(methodKey, pk,
					rating);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String insertNewIdea(HttpPrincipal httpPrincipal,
		java.lang.String title, java.lang.String category, long userId,
		java.lang.String icon, java.lang.String shortDescription,
		java.lang.String description, double latitude, double longitude,
		boolean published, boolean showOnMap, long groupId, int rating) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"insertNewIdea", _insertNewIdeaParameterTypes11);

			MethodHandler methodHandler = new MethodHandler(methodKey, title,
					category, userId, icon, shortDescription, description,
					latitude, longitude, published, showOnMap, groupId, rating);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	public static java.lang.String deleteIdea(HttpPrincipal httpPrincipal,
		java.lang.String Id) {
		try {
			MethodKey methodKey = new MethodKey(IdeasServiceUtil.class,
					"deleteIdea", _deleteIdeaParameterTypes12);

			MethodHandler methodHandler = new MethodHandler(methodKey, Id);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception e) {
				throw new com.liferay.portal.kernel.exception.SystemException(e);
			}

			return (java.lang.String)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException se) {
			_log.error(se, se);

			throw se;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(IdeasServiceHttp.class);
	private static final Class<?>[] _getAllIdeasParameterTypes0 = new Class[] {  };
	private static final Class<?>[] _getIdeaByIdParameterTypes1 = new Class[] {
			long.class
		};
	private static final Class<?>[] _getIdeasByCategoryParameterTypes2 = new Class[] {
			java.lang.String.class
		};
	private static final Class<?>[] _getIdeasByRatingParameterTypes3 = new Class[] {
			int.class
		};
	private static final Class<?>[] _getIdeasByRatingRangeParameterTypes4 = new Class[] {
			int.class, int.class
		};
	private static final Class<?>[] _searchIdeasByFieldParameterTypes5 = new Class[] {
			java.lang.String.class, java.lang.String.class, long[].class
		};
	private static final Class<?>[] _seachIdeasByFieldArrayParameterTypes6 = new Class[] {
			java.lang.String[].class, java.lang.String.class, long[].class
		};
	private static final Class<?>[] _searchIdeasByFieldArrayQueryArrayParameterTypes7 =
		new Class[] {
			java.lang.String[].class, java.lang.String[].class, long[].class
		};
	private static final Class<?>[] _getIdeasByIsPublishedParameterTypes8 = new Class[] {
			boolean.class
		};
	private static final Class<?>[] _getIdeasByIsVisibleOnMapParameterTypes9 = new Class[] {
			boolean.class
		};
	private static final Class<?>[] _updateIdeasRatingParameterTypes10 = new Class[] {
			long.class, int.class
		};
	private static final Class<?>[] _insertNewIdeaParameterTypes11 = new Class[] {
			java.lang.String.class, java.lang.String.class, long.class,
			java.lang.String.class, java.lang.String.class,
			java.lang.String.class, double.class, double.class, boolean.class,
			boolean.class, long.class, int.class
		};
	private static final Class<?>[] _deleteIdeaParameterTypes12 = new Class[] {
			java.lang.String.class
		};
}