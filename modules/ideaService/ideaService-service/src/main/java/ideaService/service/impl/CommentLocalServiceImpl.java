/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;

import aQute.bnd.annotation.ProviderType;
import ideaService.exception.NoSuchCommentException;
import ideaService.model.Comment;
import ideaService.service.CommentLocalServiceUtil;
import ideaService.service.base.CommentLocalServiceBaseImpl;
import ideaService.service.persistence.CommentUtil;

/**
 * The implementation of the comment local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link ideaService.service.CommentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see CommentLocalServiceBaseImpl
 * @see ideaService.service.CommentLocalServiceUtil
 */
@ProviderType
public class CommentLocalServiceImpl extends CommentLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link ideaService.service.CommentLocalServiceUtil} to access the comment local service.
	 */
	/**
	 * @return a new Comment with an automatically added primaryKey.
	 * Does not persist the Comment and does not perform any kind of verification.
	 */
	public Comment createCommentWithAutomatedDbId(String IdeaId, String commentText, long userId){
		long nextDbId = CounterLocalServiceUtil.increment(Comment.class.getName());
		Comment nextComment = CommentLocalServiceUtil.createComment(nextDbId);
		nextComment.setCommentText(commentText);
		nextComment.setIdeaRef(Long.parseLong(IdeaId));
		nextComment.setUserId(userId);
		return nextComment;
	}

	/**
	 * @return all Comments for a given IdeasId
	 */
	public List<Comment> getAllCommentsForIdeasId(String IdeaId){
		List<Comment> allComments = CommentUtil.findAll();
		List<Comment> result = new ArrayList<Comment>();
		for(Comment c : allComments){
			if(Long.toString(c.getIdeaRef()).equals(IdeaId)){
				result.add(c);
			}
		}
		return result;
	}

	/**
	 * @return Map with IdeasId as the key and a List with all Comments corresponding to this idea
	 */
	public Map<Long,List<Comment>> getAllCommentsWithRefIds(){
		List<Comment> comments = CommentUtil.findAll();
		Map<Long,List<Comment>> result = new HashMap<Long,List<Comment>>();
		for(Comment c: comments){
			if(result.containsKey(c.getIdeaRef())){
				List<Comment> tmp = result.get(c.getIdeaRef());
				tmp.add(c);
				result.put(c.getIdeaRef(), tmp);
			}else{
				List<Comment> tmp = new ArrayList<Comment>();
				tmp.add(c);
				result.put(c.getIdeaRef(), tmp);
			}
		}
		return result;
	}

	/**
	 * Delete all Comments with IdeaId
	 */
	public void deleteCommentWithIdeasId(long IdeaId) throws NoSuchCommentException{
		List<Comment> allComments = CommentUtil.findAll();
		if(!allComments.equals(null) && !allComments.isEmpty()){
		for(Comment c : allComments){
			if(c.getIdeaRef() == IdeaId){
				CommentUtil.remove(c.getPrimaryKey());
				}
			}
		}
	}

	/**
	 * Persist one comment object and performs all necessary type checks.
	 * Create the Comment object using createCommentWithAutomatedDbId.
	 */
	public void persistCommentAndPerformTypeChecks(Comment c){
		//TODO type and securtiy checks
		c.persist();
	}
}