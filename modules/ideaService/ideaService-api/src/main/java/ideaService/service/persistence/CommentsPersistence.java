/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import ideaService.exception.NoSuchCommentsException;

import ideaService.model.Comments;

/**
 * The persistence interface for the comments service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.persistence.impl.CommentsPersistenceImpl
 * @see CommentsUtil
 * @generated
 */
@ProviderType
public interface CommentsPersistence extends BasePersistence<Comments> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CommentsUtil} to access the comments persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the comments in the entity cache if it is enabled.
	*
	* @param comments the comments
	*/
	public void cacheResult(Comments comments);

	/**
	* Caches the commentses in the entity cache if it is enabled.
	*
	* @param commentses the commentses
	*/
	public void cacheResult(java.util.List<Comments> commentses);

	/**
	* Creates a new comments with the primary key. Does not add the comments to the database.
	*
	* @param commentId the primary key for the new comments
	* @return the new comments
	*/
	public Comments create(long commentId);

	/**
	* Removes the comments with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param commentId the primary key of the comments
	* @return the comments that was removed
	* @throws NoSuchCommentsException if a comments with the primary key could not be found
	*/
	public Comments remove(long commentId) throws NoSuchCommentsException;

	public Comments updateImpl(Comments comments);

	/**
	* Returns the comments with the primary key or throws a {@link NoSuchCommentsException} if it could not be found.
	*
	* @param commentId the primary key of the comments
	* @return the comments
	* @throws NoSuchCommentsException if a comments with the primary key could not be found
	*/
	public Comments findByPrimaryKey(long commentId)
		throws NoSuchCommentsException;

	/**
	* Returns the comments with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param commentId the primary key of the comments
	* @return the comments, or <code>null</code> if a comments with the primary key could not be found
	*/
	public Comments fetchByPrimaryKey(long commentId);

	@Override
	public java.util.Map<java.io.Serializable, Comments> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the commentses.
	*
	* @return the commentses
	*/
	public java.util.List<Comments> findAll();

	/**
	* Returns a range of all the commentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CommentsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @return the range of commentses
	*/
	public java.util.List<Comments> findAll(int start, int end);

	/**
	* Returns an ordered range of all the commentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CommentsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of commentses
	*/
	public java.util.List<Comments> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Comments> orderByComparator);

	/**
	* Returns an ordered range of all the commentses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link CommentsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of commentses
	* @param end the upper bound of the range of commentses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of commentses
	*/
	public java.util.List<Comments> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Comments> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the commentses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of commentses.
	*
	* @return the number of commentses
	*/
	public int countAll();
}