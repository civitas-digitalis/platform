/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the remote service utility for Ideas. This utility wraps
 * {@link ideaService.service.impl.IdeasServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see IdeasService
 * @see ideaService.service.base.IdeasServiceBaseImpl
 * @see ideaService.service.impl.IdeasServiceImpl
 * @generated
 */
@ProviderType
public class IdeasServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link ideaService.service.impl.IdeasServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */
	public static java.lang.String deleteIdea(java.lang.String Id) {
		return getService().deleteIdea(Id);
	}

	/**
	* finds and returns all ideas.
	*/
	public static java.lang.String getAllIdeas() {
		return getService().getAllIdeas();
	}

	public static java.lang.String getIdeaById(long id) {
		return getService().getIdeaById(id);
	}

	/**
	* Finds all ideas with Category category.
	* This is a remote function. Not to be used locally!
	*
	* @param cat the desired cat
	* @return a list with all ideas that have type equal to input param
	*/
	public static java.lang.String getIdeasByCategory(java.lang.String category) {
		return getService().getIdeasByCategory(category);
	}

	public static java.lang.String getIdeasByIsPublished(boolean published) {
		return getService().getIdeasByIsPublished(published);
	}

	public static java.lang.String getIdeasByIsVisibleOnMap(boolean visible) {
		return getService().getIdeasByIsVisibleOnMap(visible);
	}

	/**
	* @param rating
	* @return
	*/
	public static java.lang.String getIdeasByRating(int rating) {
		return getService().getIdeasByRating(rating);
	}

	/**
	* @param ratingRangeStart
	* @param ratingRangeEnd
	* @return all ideas with the given range.
	*/
	public static java.lang.String getIdeasByRatingRange(int ratingRangeStart,
		int ratingRangeEnd) {
		return getService()
				   .getIdeasByRatingRange(ratingRangeStart, ratingRangeEnd);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static java.lang.String insertNewIdea(java.lang.String title,
		java.lang.String category, long userId, java.lang.String icon,
		java.lang.String shortDescription, java.lang.String description,
		double latitude, double longitude, boolean published,
		boolean showOnMap, long groupId, int rating) {
		return getService()
				   .insertNewIdea(title, category, userId, icon,
			shortDescription, description, latitude, longitude, published,
			showOnMap, groupId, rating);
	}

	public static java.lang.String seachIdeasByFieldArray(
		java.lang.String[] fieldNames, java.lang.String query, long[] groupIds) {
		return getService().seachIdeasByFieldArray(fieldNames, query, groupIds);
	}

	public static java.lang.String searchIdeasByField(
		java.lang.String fieldName, java.lang.String query, long[] groupIds) {
		return getService().searchIdeasByField(fieldName, query, groupIds);
	}

	public static java.lang.String searchIdeasByFieldArrayQueryArray(
		java.lang.String[] fieldNames, java.lang.String[] queries,
		long[] groupIds) {
		return getService()
				   .searchIdeasByFieldArrayQueryArray(fieldNames, queries,
			groupIds);
	}

	/**
	* @param pk the primaryKey
	* @param rating the rating to set
	* @return reponse json
	*/
	public static java.lang.String updateIdeasRating(long pk, int rating) {
		return getService().updateIdeasRating(pk, rating);
	}

	public static IdeasService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<IdeasService, IdeasService> _serviceTracker = ServiceTrackerFactory.open(IdeasService.class);
}