/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import ideaService.exception.NoSuchIdeasException;

import ideaService.model.Ideas;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for Ideas. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see IdeasLocalServiceUtil
 * @see ideaService.service.base.IdeasLocalServiceBaseImpl
 * @see ideaService.service.impl.IdeasLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface IdeasLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link IdeasLocalServiceUtil} to access the ideas local service. Add custom service methods to {@link ideaService.service.impl.IdeasLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* Adds the ideas to the database. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Ideas addIdeas(Ideas ideas);

	/**
	* Creates a new Idea using the supplied parameters.
	* created date, modified date and user name are set automatically.
	*/
	public Ideas creadeIdeasWithAutomatedDbId(java.lang.String title,
		long userId, long groupId, java.lang.String type,
		java.lang.String icon, java.lang.String shortDescription,
		java.lang.String description, double latitude, double longitude,
		boolean published, boolean showOnMap, int rating);

	/**
	* Creates a new ideas with the primary key. Does not add the ideas to the database.
	*
	* @param ideasId the primary key for the new ideas
	* @return the new ideas
	*/
	public Ideas createIdeas(long ideasId);

	/**
	* Deletes the ideas from the database. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Ideas deleteIdeas(Ideas ideas);

	/**
	* Deletes the ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas that was removed
	* @throws PortalException if a ideas with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Ideas deleteIdeas(long ideasId) throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Ideas fetchIdeas(long ideasId);

	/**
	* Returns the ideas with the primary key.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas
	* @throws PortalException if a ideas with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Ideas getIdeas(long ideasId) throws PortalException;

	/**
	* Updates the ideas in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Ideas updateIdeas(Ideas ideas);

	/**
	* Returns the number of ideases.
	*
	* @return the number of ideases
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getIdeasesCount();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	/**
	* returns all ideas as a list.
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getAllIdeas();

	/**
	* Finds all ideas with Category cat.
	*
	* @param cat the desired cat
	* @return a list with all ideas that have type equal to input param
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasByCategory(java.lang.String cat);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasByIsPublished(boolean published);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasByIsVisibleOnMap(boolean visible);

	/**
	* Find all ideas with Rating rating.
	*
	* @param rating the exact rating
	* @return a list with all ideas that have rating equal to param
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasByRating(int rating);

	/**
	* @param ratingRangeStart the start of the rating range.
	* @param ratingRangeEnd the end of the rating range.
	* @return a list with all ideas that are within the given range.
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeasByRatingRange(int ratingRangeStart,
		int ratingRangeEnd);

	/**
	* Returns a range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of ideases
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> getIdeases(int start, int end);

	/**
	* Searches the DB for the given idea with a field that contains the query.
	* Only returns a result if the query is found in all given fields.
	* Only supports search within text fields.
	*
	* @param fieldNames
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	public List<Ideas> seachIdeasByFieldArray(java.lang.String[] fieldNames,
		java.lang.String query, long[] groupIds);

	/**
	* Searches the DB forthe given idea with a field that contains the query.
	* Only supports search within text fields.
	*
	* @param fieldName
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> searchIdeasByField(java.lang.String fieldName,
		java.lang.String query, long[] groupIds);

	/**
	* Searches the DB for any field that contains the query.
	* Only supports search within text fields.
	*
	* @param fieldNames
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Ideas> searchIdeasByFieldArrayQueryArray(
		java.lang.String[] fieldNames, java.lang.String[] queries,
		long[] groupIds);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);

	/**
	* Deletes the idea with param id.
	*
	* @param id
	*/
	public void deleteIdea(long id) throws NoSuchIdeasException;

	/**
	* persists the new Idea and performs some checks e.g. if the entry is a duplicate it won't be inserted.
	*
	* @param idea
	*/
	public void persistIdeasAndPerformTypeChecks(Ideas idea);

	/**
	* sets the rating of the idea with primaryKey pk
	*
	* @param pk the primary key
	* @param rating the rating to set
	*/
	public void setRating(long pk, int rating);
}