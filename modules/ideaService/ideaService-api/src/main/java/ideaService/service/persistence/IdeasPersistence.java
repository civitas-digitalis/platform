/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import ideaService.exception.NoSuchIdeasException;

import ideaService.model.Ideas;

/**
 * The persistence interface for the ideas service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.persistence.impl.IdeasPersistenceImpl
 * @see IdeasUtil
 * @generated
 */
@ProviderType
public interface IdeasPersistence extends BasePersistence<Ideas> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link IdeasUtil} to access the ideas persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the ideas in the entity cache if it is enabled.
	*
	* @param ideas the ideas
	*/
	public void cacheResult(Ideas ideas);

	/**
	* Caches the ideases in the entity cache if it is enabled.
	*
	* @param ideases the ideases
	*/
	public void cacheResult(java.util.List<Ideas> ideases);

	/**
	* Creates a new ideas with the primary key. Does not add the ideas to the database.
	*
	* @param ideasId the primary key for the new ideas
	* @return the new ideas
	*/
	public Ideas create(long ideasId);

	/**
	* Removes the ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas that was removed
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public Ideas remove(long ideasId) throws NoSuchIdeasException;

	public Ideas updateImpl(Ideas ideas);

	/**
	* Returns the ideas with the primary key or throws a {@link NoSuchIdeasException} if it could not be found.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas
	* @throws NoSuchIdeasException if a ideas with the primary key could not be found
	*/
	public Ideas findByPrimaryKey(long ideasId) throws NoSuchIdeasException;

	/**
	* Returns the ideas with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas, or <code>null</code> if a ideas with the primary key could not be found
	*/
	public Ideas fetchByPrimaryKey(long ideasId);

	@Override
	public java.util.Map<java.io.Serializable, Ideas> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the ideases.
	*
	* @return the ideases
	*/
	public java.util.List<Ideas> findAll();

	/**
	* Returns a range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of ideases
	*/
	public java.util.List<Ideas> findAll(int start, int end);

	/**
	* Returns an ordered range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ideases
	*/
	public java.util.List<Ideas> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator);

	/**
	* Returns an ordered range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of ideases
	*/
	public java.util.List<Ideas> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Ideas> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the ideases from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of ideases.
	*
	* @return the number of ideases
	*/
	public int countAll();
}