/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link IdeasService}.
 *
 * @author Brian Wing Shun Chan
 * @see IdeasService
 * @generated
 */
@ProviderType
public class IdeasServiceWrapper implements IdeasService,
	ServiceWrapper<IdeasService> {
	public IdeasServiceWrapper(IdeasService ideasService) {
		_ideasService = ideasService;
	}

	@Override
	public java.lang.String deleteIdea(java.lang.String Id) {
		return _ideasService.deleteIdea(Id);
	}

	/**
	* finds and returns all ideas.
	*/
	@Override
	public java.lang.String getAllIdeas() {
		return _ideasService.getAllIdeas();
	}

	@Override
	public java.lang.String getIdeaById(long id) {
		return _ideasService.getIdeaById(id);
	}

	/**
	* Finds all ideas with Category category.
	* This is a remote function. Not to be used locally!
	*
	* @param cat the desired cat
	* @return a list with all ideas that have type equal to input param
	*/
	@Override
	public java.lang.String getIdeasByCategory(java.lang.String category) {
		return _ideasService.getIdeasByCategory(category);
	}

	@Override
	public java.lang.String getIdeasByIsPublished(boolean published) {
		return _ideasService.getIdeasByIsPublished(published);
	}

	@Override
	public java.lang.String getIdeasByIsVisibleOnMap(boolean visible) {
		return _ideasService.getIdeasByIsVisibleOnMap(visible);
	}

	/**
	* @param rating
	* @return
	*/
	@Override
	public java.lang.String getIdeasByRating(int rating) {
		return _ideasService.getIdeasByRating(rating);
	}

	/**
	* @param ratingRangeStart
	* @param ratingRangeEnd
	* @return all ideas with the given range.
	*/
	@Override
	public java.lang.String getIdeasByRatingRange(int ratingRangeStart,
		int ratingRangeEnd) {
		return _ideasService.getIdeasByRatingRange(ratingRangeStart,
			ratingRangeEnd);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _ideasService.getOSGiServiceIdentifier();
	}

	@Override
	public java.lang.String insertNewIdea(java.lang.String title,
		java.lang.String category, long userId, java.lang.String icon,
		java.lang.String shortDescription, java.lang.String description,
		double latitude, double longitude, boolean published,
		boolean showOnMap, long groupId, int rating) {
		return _ideasService.insertNewIdea(title, category, userId, icon,
			shortDescription, description, latitude, longitude, published,
			showOnMap, groupId, rating);
	}

	@Override
	public java.lang.String seachIdeasByFieldArray(
		java.lang.String[] fieldNames, java.lang.String query, long[] groupIds) {
		return _ideasService.seachIdeasByFieldArray(fieldNames, query, groupIds);
	}

	@Override
	public java.lang.String searchIdeasByField(java.lang.String fieldName,
		java.lang.String query, long[] groupIds) {
		return _ideasService.searchIdeasByField(fieldName, query, groupIds);
	}

	@Override
	public java.lang.String searchIdeasByFieldArrayQueryArray(
		java.lang.String[] fieldNames, java.lang.String[] queries,
		long[] groupIds) {
		return _ideasService.searchIdeasByFieldArrayQueryArray(fieldNames,
			queries, groupIds);
	}

	/**
	* @param pk the primaryKey
	* @param rating the rating to set
	* @return reponse json
	*/
	@Override
	public java.lang.String updateIdeasRating(long pk, int rating) {
		return _ideasService.updateIdeasRating(pk, rating);
	}

	@Override
	public IdeasService getWrappedService() {
		return _ideasService;
	}

	@Override
	public void setWrappedService(IdeasService ideasService) {
		_ideasService = ideasService;
	}

	private IdeasService _ideasService;
}