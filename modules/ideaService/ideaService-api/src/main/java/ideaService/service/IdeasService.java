/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.security.access.control.AccessControlled;
import com.liferay.portal.kernel.service.BaseService;
import com.liferay.portal.kernel.spring.osgi.OSGiBeanProperties;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * Provides the remote service interface for Ideas. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see IdeasServiceUtil
 * @see ideaService.service.base.IdeasServiceBaseImpl
 * @see ideaService.service.impl.IdeasServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@OSGiBeanProperties(property =  {
	"json.web.service.context.name=idea", "json.web.service.context.path=Ideas"}, service = IdeasService.class)
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface IdeasService extends BaseService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link IdeasServiceUtil} to access the ideas remote service. Add custom service methods to {@link ideaService.service.impl.IdeasServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	@JSONWebService(method = "DELETE")
	public java.lang.String deleteIdea(java.lang.String Id);

	/**
	* finds and returns all ideas.
	*/
	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getAllIdeas();

	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getIdeaById(long id);

	/**
	* Finds all ideas with Category category.
	* This is a remote function. Not to be used locally!
	*
	* @param cat the desired cat
	* @return a list with all ideas that have type equal to input param
	*/
	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getIdeasByCategory(java.lang.String category);

	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getIdeasByIsPublished(boolean published);

	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getIdeasByIsVisibleOnMap(boolean visible);

	/**
	* @param rating
	* @return
	*/
	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getIdeasByRating(int rating);

	/**
	* @param ratingRangeStart
	* @param ratingRangeEnd
	* @return all ideas with the given range.
	*/
	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String getIdeasByRatingRange(int ratingRangeStart,
		int ratingRangeEnd);

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	@JSONWebService(method = "POST")
	public java.lang.String insertNewIdea(java.lang.String title,
		java.lang.String category, long userId, java.lang.String icon,
		java.lang.String shortDescription, java.lang.String description,
		double latitude, double longitude, boolean published,
		boolean showOnMap, long groupId, int rating);

	@JSONWebService(method = "GET")
	public java.lang.String seachIdeasByFieldArray(
		java.lang.String[] fieldNames, java.lang.String query, long[] groupIds);

	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String searchIdeasByField(java.lang.String fieldName,
		java.lang.String query, long[] groupIds);

	@JSONWebService(method = "GET")
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public java.lang.String searchIdeasByFieldArrayQueryArray(
		java.lang.String[] fieldNames, java.lang.String[] queries,
		long[] groupIds);

	/**
	* @param pk the primaryKey
	* @param rating the rating to set
	* @return reponse json
	*/
	@JSONWebService(method = "PUT")
	public java.lang.String updateIdeasRating(long pk, int rating);
}