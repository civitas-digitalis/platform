/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link IdeasLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see IdeasLocalService
 * @generated
 */
@ProviderType
public class IdeasLocalServiceWrapper implements IdeasLocalService,
	ServiceWrapper<IdeasLocalService> {
	public IdeasLocalServiceWrapper(IdeasLocalService ideasLocalService) {
		_ideasLocalService = ideasLocalService;
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _ideasLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ideasLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _ideasLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ideasLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ideasLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Adds the ideas to the database. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was added
	*/
	@Override
	public ideaService.model.Ideas addIdeas(ideaService.model.Ideas ideas) {
		return _ideasLocalService.addIdeas(ideas);
	}

	/**
	* Creates a new Idea using the supplied parameters.
	* created date, modified date and user name are set automatically.
	*/
	@Override
	public ideaService.model.Ideas creadeIdeasWithAutomatedDbId(
		java.lang.String title, long userId, long groupId,
		java.lang.String type, java.lang.String icon,
		java.lang.String shortDescription, java.lang.String description,
		double latitude, double longitude, boolean published,
		boolean showOnMap, int rating) {
		return _ideasLocalService.creadeIdeasWithAutomatedDbId(title, userId,
			groupId, type, icon, shortDescription, description, latitude,
			longitude, published, showOnMap, rating);
	}

	/**
	* Creates a new ideas with the primary key. Does not add the ideas to the database.
	*
	* @param ideasId the primary key for the new ideas
	* @return the new ideas
	*/
	@Override
	public ideaService.model.Ideas createIdeas(long ideasId) {
		return _ideasLocalService.createIdeas(ideasId);
	}

	/**
	* Deletes the ideas from the database. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was removed
	*/
	@Override
	public ideaService.model.Ideas deleteIdeas(ideaService.model.Ideas ideas) {
		return _ideasLocalService.deleteIdeas(ideas);
	}

	/**
	* Deletes the ideas with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas that was removed
	* @throws PortalException if a ideas with the primary key could not be found
	*/
	@Override
	public ideaService.model.Ideas deleteIdeas(long ideasId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ideasLocalService.deleteIdeas(ideasId);
	}

	@Override
	public ideaService.model.Ideas fetchIdeas(long ideasId) {
		return _ideasLocalService.fetchIdeas(ideasId);
	}

	/**
	* Returns the ideas with the primary key.
	*
	* @param ideasId the primary key of the ideas
	* @return the ideas
	* @throws PortalException if a ideas with the primary key could not be found
	*/
	@Override
	public ideaService.model.Ideas getIdeas(long ideasId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ideasLocalService.getIdeas(ideasId);
	}

	/**
	* Updates the ideas in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ideas the ideas
	* @return the ideas that was updated
	*/
	@Override
	public ideaService.model.Ideas updateIdeas(ideaService.model.Ideas ideas) {
		return _ideasLocalService.updateIdeas(ideas);
	}

	/**
	* Returns the number of ideases.
	*
	* @return the number of ideases
	*/
	@Override
	public int getIdeasesCount() {
		return _ideasLocalService.getIdeasesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _ideasLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ideasLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _ideasLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _ideasLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* returns all ideas as a list.
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getAllIdeas() {
		return _ideasLocalService.getAllIdeas();
	}

	/**
	* Finds all ideas with Category cat.
	*
	* @param cat the desired cat
	* @return a list with all ideas that have type equal to input param
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasByCategory(
		java.lang.String cat) {
		return _ideasLocalService.getIdeasByCategory(cat);
	}

	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasByIsPublished(
		boolean published) {
		return _ideasLocalService.getIdeasByIsPublished(published);
	}

	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasByIsVisibleOnMap(
		boolean visible) {
		return _ideasLocalService.getIdeasByIsVisibleOnMap(visible);
	}

	/**
	* Find all ideas with Rating rating.
	*
	* @param rating the exact rating
	* @return a list with all ideas that have rating equal to param
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasByRating(int rating) {
		return _ideasLocalService.getIdeasByRating(rating);
	}

	/**
	* @param ratingRangeStart the start of the rating range.
	* @param ratingRangeEnd the end of the rating range.
	* @return a list with all ideas that are within the given range.
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeasByRatingRange(
		int ratingRangeStart, int ratingRangeEnd) {
		return _ideasLocalService.getIdeasByRatingRange(ratingRangeStart,
			ratingRangeEnd);
	}

	/**
	* Returns a range of all the ideases.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ideaService.model.impl.IdeasModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ideases
	* @param end the upper bound of the range of ideases (not inclusive)
	* @return the range of ideases
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> getIdeases(int start, int end) {
		return _ideasLocalService.getIdeases(start, end);
	}

	/**
	* Searches the DB for the given idea with a field that contains the query.
	* Only returns a result if the query is found in all given fields.
	* Only supports search within text fields.
	*
	* @param fieldNames
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> seachIdeasByFieldArray(
		java.lang.String[] fieldNames, java.lang.String query, long[] groupIds) {
		return _ideasLocalService.seachIdeasByFieldArray(fieldNames, query,
			groupIds);
	}

	/**
	* Searches the DB forthe given idea with a field that contains the query.
	* Only supports search within text fields.
	*
	* @param fieldName
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> searchIdeasByField(
		java.lang.String fieldName, java.lang.String query, long[] groupIds) {
		return _ideasLocalService.searchIdeasByField(fieldName, query, groupIds);
	}

	/**
	* Searches the DB for any field that contains the query.
	* Only supports search within text fields.
	*
	* @param fieldNames
	* @param query
	* @param groupIds
	* @return a list of ideas with each containing the query.
	*/
	@Override
	public java.util.List<ideaService.model.Ideas> searchIdeasByFieldArrayQueryArray(
		java.lang.String[] fieldNames, java.lang.String[] queries,
		long[] groupIds) {
		return _ideasLocalService.searchIdeasByFieldArrayQueryArray(fieldNames,
			queries, groupIds);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ideasLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _ideasLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	/**
	* Deletes the idea with param id.
	*
	* @param id
	*/
	@Override
	public void deleteIdea(long id)
		throws ideaService.exception.NoSuchIdeasException {
		_ideasLocalService.deleteIdea(id);
	}

	/**
	* persists the new Idea and performs some checks e.g. if the entry is a duplicate it won't be inserted.
	*
	* @param idea
	*/
	@Override
	public void persistIdeasAndPerformTypeChecks(ideaService.model.Ideas idea) {
		_ideasLocalService.persistIdeasAndPerformTypeChecks(idea);
	}

	/**
	* sets the rating of the idea with primaryKey pk
	*
	* @param pk the primary key
	* @param rating the rating to set
	*/
	@Override
	public void setRating(long pk, int rating) {
		_ideasLocalService.setRating(pk, rating);
	}

	@Override
	public IdeasLocalService getWrappedService() {
		return _ideasLocalService;
	}

	@Override
	public void setWrappedService(IdeasLocalService ideasLocalService) {
		_ideasLocalService = ideasLocalService;
	}

	private IdeasLocalService _ideasLocalService;
}