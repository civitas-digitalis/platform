/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link ideaService.service.http.CommentsServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.http.CommentsServiceSoap
 * @generated
 */
@ProviderType
public class CommentsSoap implements Serializable {
	public static CommentsSoap toSoapModel(Comments model) {
		CommentsSoap soapModel = new CommentsSoap();

		soapModel.setCommentId(model.getCommentId());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setIdeaRef(model.getIdeaRef());
		soapModel.setCommentRef(model.getCommentRef());
		soapModel.setCommentText(model.getCommentText());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());

		return soapModel;
	}

	public static CommentsSoap[] toSoapModels(Comments[] models) {
		CommentsSoap[] soapModels = new CommentsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CommentsSoap[][] toSoapModels(Comments[][] models) {
		CommentsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CommentsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CommentsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CommentsSoap[] toSoapModels(List<Comments> models) {
		List<CommentsSoap> soapModels = new ArrayList<CommentsSoap>(models.size());

		for (Comments model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CommentsSoap[soapModels.size()]);
	}

	public CommentsSoap() {
	}

	public long getPrimaryKey() {
		return _commentId;
	}

	public void setPrimaryKey(long pk) {
		setCommentId(pk);
	}

	public long getCommentId() {
		return _commentId;
	}

	public void setCommentId(long commentId) {
		_commentId = commentId;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getIdeaRef() {
		return _IdeaRef;
	}

	public void setIdeaRef(long IdeaRef) {
		_IdeaRef = IdeaRef;
	}

	public long getCommentRef() {
		return _commentRef;
	}

	public void setCommentRef(long commentRef) {
		_commentRef = commentRef;
	}

	public String getCommentText() {
		return _commentText;
	}

	public void setCommentText(String commentText) {
		_commentText = commentText;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	private long _commentId;
	private Date _createDate;
	private Date _modifiedDate;
	private long _companyId;
	private long _groupId;
	private long _IdeaRef;
	private long _commentRef;
	private String _commentText;
	private long _userId;
	private String _userName;
}