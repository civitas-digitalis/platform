/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Comments}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Comments
 * @generated
 */
@ProviderType
public class CommentsWrapper implements Comments, ModelWrapper<Comments> {
	public CommentsWrapper(Comments comments) {
		_comments = comments;
	}

	@Override
	public Class<?> getModelClass() {
		return Comments.class;
	}

	@Override
	public String getModelClassName() {
		return Comments.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("commentId", getCommentId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("IdeaRef", getIdeaRef());
		attributes.put("commentRef", getCommentRef());
		attributes.put("commentText", getCommentText());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long commentId = (Long)attributes.get("commentId");

		if (commentId != null) {
			setCommentId(commentId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long IdeaRef = (Long)attributes.get("IdeaRef");

		if (IdeaRef != null) {
			setIdeaRef(IdeaRef);
		}

		Long commentRef = (Long)attributes.get("commentRef");

		if (commentRef != null) {
			setCommentRef(commentRef);
		}

		String commentText = (String)attributes.get("commentText");

		if (commentText != null) {
			setCommentText(commentText);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _comments.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _comments.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _comments.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _comments.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ideaService.model.Comments> toCacheModel() {
		return _comments.toCacheModel();
	}

	@Override
	public ideaService.model.Comments toEscapedModel() {
		return new CommentsWrapper(_comments.toEscapedModel());
	}

	@Override
	public ideaService.model.Comments toUnescapedModel() {
		return new CommentsWrapper(_comments.toUnescapedModel());
	}

	@Override
	public int compareTo(ideaService.model.Comments comments) {
		return _comments.compareTo(comments);
	}

	@Override
	public int hashCode() {
		return _comments.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _comments.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new CommentsWrapper((Comments)_comments.clone());
	}

	/**
	* Returns the comment text of this comments.
	*
	* @return the comment text of this comments
	*/
	@Override
	public java.lang.String getCommentText() {
		return _comments.getCommentText();
	}

	/**
	* Returns the user name of this comments.
	*
	* @return the user name of this comments
	*/
	@Override
	public java.lang.String getUserName() {
		return _comments.getUserName();
	}

	/**
	* Returns the user uuid of this comments.
	*
	* @return the user uuid of this comments
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _comments.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _comments.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _comments.toXmlString();
	}

	/**
	* Returns the create date of this comments.
	*
	* @return the create date of this comments
	*/
	@Override
	public Date getCreateDate() {
		return _comments.getCreateDate();
	}

	/**
	* Returns the modified date of this comments.
	*
	* @return the modified date of this comments
	*/
	@Override
	public Date getModifiedDate() {
		return _comments.getModifiedDate();
	}

	/**
	* Returns the comment ID of this comments.
	*
	* @return the comment ID of this comments
	*/
	@Override
	public long getCommentId() {
		return _comments.getCommentId();
	}

	/**
	* Returns the comment ref of this comments.
	*
	* @return the comment ref of this comments
	*/
	@Override
	public long getCommentRef() {
		return _comments.getCommentRef();
	}

	/**
	* Returns the company ID of this comments.
	*
	* @return the company ID of this comments
	*/
	@Override
	public long getCompanyId() {
		return _comments.getCompanyId();
	}

	/**
	* Returns the group ID of this comments.
	*
	* @return the group ID of this comments
	*/
	@Override
	public long getGroupId() {
		return _comments.getGroupId();
	}

	/**
	* Returns the idea ref of this comments.
	*
	* @return the idea ref of this comments
	*/
	@Override
	public long getIdeaRef() {
		return _comments.getIdeaRef();
	}

	/**
	* Returns the primary key of this comments.
	*
	* @return the primary key of this comments
	*/
	@Override
	public long getPrimaryKey() {
		return _comments.getPrimaryKey();
	}

	/**
	* Returns the user ID of this comments.
	*
	* @return the user ID of this comments
	*/
	@Override
	public long getUserId() {
		return _comments.getUserId();
	}

	@Override
	public void persist() {
		_comments.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_comments.setCachedModel(cachedModel);
	}

	/**
	* Sets the comment ID of this comments.
	*
	* @param commentId the comment ID of this comments
	*/
	@Override
	public void setCommentId(long commentId) {
		_comments.setCommentId(commentId);
	}

	/**
	* Sets the comment ref of this comments.
	*
	* @param commentRef the comment ref of this comments
	*/
	@Override
	public void setCommentRef(long commentRef) {
		_comments.setCommentRef(commentRef);
	}

	/**
	* Sets the comment text of this comments.
	*
	* @param commentText the comment text of this comments
	*/
	@Override
	public void setCommentText(java.lang.String commentText) {
		_comments.setCommentText(commentText);
	}

	/**
	* Sets the company ID of this comments.
	*
	* @param companyId the company ID of this comments
	*/
	@Override
	public void setCompanyId(long companyId) {
		_comments.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this comments.
	*
	* @param createDate the create date of this comments
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_comments.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_comments.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_comments.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_comments.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this comments.
	*
	* @param groupId the group ID of this comments
	*/
	@Override
	public void setGroupId(long groupId) {
		_comments.setGroupId(groupId);
	}

	/**
	* Sets the idea ref of this comments.
	*
	* @param IdeaRef the idea ref of this comments
	*/
	@Override
	public void setIdeaRef(long IdeaRef) {
		_comments.setIdeaRef(IdeaRef);
	}

	/**
	* Sets the modified date of this comments.
	*
	* @param modifiedDate the modified date of this comments
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_comments.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_comments.setNew(n);
	}

	/**
	* Sets the primary key of this comments.
	*
	* @param primaryKey the primary key of this comments
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_comments.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_comments.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this comments.
	*
	* @param userId the user ID of this comments
	*/
	@Override
	public void setUserId(long userId) {
		_comments.setUserId(userId);
	}

	/**
	* Sets the user name of this comments.
	*
	* @param userName the user name of this comments
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_comments.setUserName(userName);
	}

	/**
	* Sets the user uuid of this comments.
	*
	* @param userUuid the user uuid of this comments
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_comments.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CommentsWrapper)) {
			return false;
		}

		CommentsWrapper commentsWrapper = (CommentsWrapper)obj;

		if (Objects.equals(_comments, commentsWrapper._comments)) {
			return true;
		}

		return false;
	}

	@Override
	public Comments getWrappedModel() {
		return _comments;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _comments.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _comments.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_comments.resetOriginalValues();
	}

	private final Comments _comments;
}