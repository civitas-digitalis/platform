/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Comment}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Comment
 * @generated
 */
@ProviderType
public class CommentWrapper implements Comment, ModelWrapper<Comment> {
	public CommentWrapper(Comment comment) {
		_comment = comment;
	}

	@Override
	public Class<?> getModelClass() {
		return Comment.class;
	}

	@Override
	public String getModelClassName() {
		return Comment.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("commentId", getCommentId());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("companyId", getCompanyId());
		attributes.put("groupId", getGroupId());
		attributes.put("IdeaRef", getIdeaRef());
		attributes.put("commentRef", getCommentRef());
		attributes.put("commentText", getCommentText());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long commentId = (Long)attributes.get("commentId");

		if (commentId != null) {
			setCommentId(commentId);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long IdeaRef = (Long)attributes.get("IdeaRef");

		if (IdeaRef != null) {
			setIdeaRef(IdeaRef);
		}

		Long commentRef = (Long)attributes.get("commentRef");

		if (commentRef != null) {
			setCommentRef(commentRef);
		}

		String commentText = (String)attributes.get("commentText");

		if (commentText != null) {
			setCommentText(commentText);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}
	}

	@Override
	public boolean isCachedModel() {
		return _comment.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _comment.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _comment.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _comment.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ideaService.model.Comment> toCacheModel() {
		return _comment.toCacheModel();
	}

	@Override
	public ideaService.model.Comment toEscapedModel() {
		return new CommentWrapper(_comment.toEscapedModel());
	}

	@Override
	public ideaService.model.Comment toUnescapedModel() {
		return new CommentWrapper(_comment.toUnescapedModel());
	}

	@Override
	public int compareTo(ideaService.model.Comment comment) {
		return _comment.compareTo(comment);
	}

	@Override
	public int hashCode() {
		return _comment.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _comment.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new CommentWrapper((Comment)_comment.clone());
	}

	/**
	* Returns the comment text of this comment.
	*
	* @return the comment text of this comment
	*/
	@Override
	public java.lang.String getCommentText() {
		return _comment.getCommentText();
	}

	/**
	* Returns the user name of this comment.
	*
	* @return the user name of this comment
	*/
	@Override
	public java.lang.String getUserName() {
		return _comment.getUserName();
	}

	/**
	* Returns the user uuid of this comment.
	*
	* @return the user uuid of this comment
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _comment.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _comment.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _comment.toXmlString();
	}

	/**
	* Returns the create date of this comment.
	*
	* @return the create date of this comment
	*/
	@Override
	public Date getCreateDate() {
		return _comment.getCreateDate();
	}

	/**
	* Returns the modified date of this comment.
	*
	* @return the modified date of this comment
	*/
	@Override
	public Date getModifiedDate() {
		return _comment.getModifiedDate();
	}

	/**
	* Returns the comment ID of this comment.
	*
	* @return the comment ID of this comment
	*/
	@Override
	public long getCommentId() {
		return _comment.getCommentId();
	}

	/**
	* Returns the comment ref of this comment.
	*
	* @return the comment ref of this comment
	*/
	@Override
	public long getCommentRef() {
		return _comment.getCommentRef();
	}

	/**
	* Returns the company ID of this comment.
	*
	* @return the company ID of this comment
	*/
	@Override
	public long getCompanyId() {
		return _comment.getCompanyId();
	}

	/**
	* Returns the group ID of this comment.
	*
	* @return the group ID of this comment
	*/
	@Override
	public long getGroupId() {
		return _comment.getGroupId();
	}

	/**
	* Returns the idea ref of this comment.
	*
	* @return the idea ref of this comment
	*/
	@Override
	public long getIdeaRef() {
		return _comment.getIdeaRef();
	}

	/**
	* Returns the primary key of this comment.
	*
	* @return the primary key of this comment
	*/
	@Override
	public long getPrimaryKey() {
		return _comment.getPrimaryKey();
	}

	/**
	* Returns the user ID of this comment.
	*
	* @return the user ID of this comment
	*/
	@Override
	public long getUserId() {
		return _comment.getUserId();
	}

	@Override
	public void persist() {
		_comment.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_comment.setCachedModel(cachedModel);
	}

	/**
	* Sets the comment ID of this comment.
	*
	* @param commentId the comment ID of this comment
	*/
	@Override
	public void setCommentId(long commentId) {
		_comment.setCommentId(commentId);
	}

	/**
	* Sets the comment ref of this comment.
	*
	* @param commentRef the comment ref of this comment
	*/
	@Override
	public void setCommentRef(long commentRef) {
		_comment.setCommentRef(commentRef);
	}

	/**
	* Sets the comment text of this comment.
	*
	* @param commentText the comment text of this comment
	*/
	@Override
	public void setCommentText(java.lang.String commentText) {
		_comment.setCommentText(commentText);
	}

	/**
	* Sets the company ID of this comment.
	*
	* @param companyId the company ID of this comment
	*/
	@Override
	public void setCompanyId(long companyId) {
		_comment.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this comment.
	*
	* @param createDate the create date of this comment
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_comment.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_comment.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_comment.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_comment.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this comment.
	*
	* @param groupId the group ID of this comment
	*/
	@Override
	public void setGroupId(long groupId) {
		_comment.setGroupId(groupId);
	}

	/**
	* Sets the idea ref of this comment.
	*
	* @param IdeaRef the idea ref of this comment
	*/
	@Override
	public void setIdeaRef(long IdeaRef) {
		_comment.setIdeaRef(IdeaRef);
	}

	/**
	* Sets the modified date of this comment.
	*
	* @param modifiedDate the modified date of this comment
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_comment.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_comment.setNew(n);
	}

	/**
	* Sets the primary key of this comment.
	*
	* @param primaryKey the primary key of this comment
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_comment.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_comment.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the user ID of this comment.
	*
	* @param userId the user ID of this comment
	*/
	@Override
	public void setUserId(long userId) {
		_comment.setUserId(userId);
	}

	/**
	* Sets the user name of this comment.
	*
	* @param userName the user name of this comment
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_comment.setUserName(userName);
	}

	/**
	* Sets the user uuid of this comment.
	*
	* @param userUuid the user uuid of this comment
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_comment.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CommentWrapper)) {
			return false;
		}

		CommentWrapper commentWrapper = (CommentWrapper)obj;

		if (Objects.equals(_comment, commentWrapper._comment)) {
			return true;
		}

		return false;
	}

	@Override
	public Comment getWrappedModel() {
		return _comment;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _comment.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _comment.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_comment.resetOriginalValues();
	}

	private final Comment _comment;
}