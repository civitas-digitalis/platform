/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Ideas}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Ideas
 * @generated
 */
@ProviderType
public class IdeasWrapper implements Ideas, ModelWrapper<Ideas> {
	public IdeasWrapper(Ideas ideas) {
		_ideas = ideas;
	}

	@Override
	public Class<?> getModelClass() {
		return Ideas.class;
	}

	@Override
	public String getModelClassName() {
		return Ideas.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ideasId", getIdeasId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("groupId", getGroupId());
		attributes.put("title", getTitle());
		attributes.put("description", getDescription());
		attributes.put("shortdescription", getShortdescription());
		attributes.put("category", getCategory());
		attributes.put("latitude", getLatitude());
		attributes.put("longitude", getLongitude());
		attributes.put("icon", getIcon());
		attributes.put("published", getPublished());
		attributes.put("isVisibleOnMap", getIsVisibleOnMap());
		attributes.put("rating", getRating());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ideasId = (Long)attributes.get("ideasId");

		if (ideasId != null) {
			setIdeasId(ideasId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String shortdescription = (String)attributes.get("shortdescription");

		if (shortdescription != null) {
			setShortdescription(shortdescription);
		}

		String category = (String)attributes.get("category");

		if (category != null) {
			setCategory(category);
		}

		Double latitude = (Double)attributes.get("latitude");

		if (latitude != null) {
			setLatitude(latitude);
		}

		Double longitude = (Double)attributes.get("longitude");

		if (longitude != null) {
			setLongitude(longitude);
		}

		String icon = (String)attributes.get("icon");

		if (icon != null) {
			setIcon(icon);
		}

		Boolean published = (Boolean)attributes.get("published");

		if (published != null) {
			setPublished(published);
		}

		Boolean isVisibleOnMap = (Boolean)attributes.get("isVisibleOnMap");

		if (isVisibleOnMap != null) {
			setIsVisibleOnMap(isVisibleOnMap);
		}

		Integer rating = (Integer)attributes.get("rating");

		if (rating != null) {
			setRating(rating);
		}
	}

	/**
	* Returns the is visible on map of this ideas.
	*
	* @return the is visible on map of this ideas
	*/
	@Override
	public boolean getIsVisibleOnMap() {
		return _ideas.getIsVisibleOnMap();
	}

	/**
	* Returns the published of this ideas.
	*
	* @return the published of this ideas
	*/
	@Override
	public boolean getPublished() {
		return _ideas.getPublished();
	}

	@Override
	public boolean isCachedModel() {
		return _ideas.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _ideas.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this ideas is is visible on map.
	*
	* @return <code>true</code> if this ideas is is visible on map; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsVisibleOnMap() {
		return _ideas.isIsVisibleOnMap();
	}

	@Override
	public boolean isNew() {
		return _ideas.isNew();
	}

	/**
	* Returns <code>true</code> if this ideas is published.
	*
	* @return <code>true</code> if this ideas is published; <code>false</code> otherwise
	*/
	@Override
	public boolean isPublished() {
		return _ideas.isPublished();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _ideas.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ideaService.model.Ideas> toCacheModel() {
		return _ideas.toCacheModel();
	}

	/**
	* Returns the latitude of this ideas.
	*
	* @return the latitude of this ideas
	*/
	@Override
	public double getLatitude() {
		return _ideas.getLatitude();
	}

	/**
	* Returns the longitude of this ideas.
	*
	* @return the longitude of this ideas
	*/
	@Override
	public double getLongitude() {
		return _ideas.getLongitude();
	}

	@Override
	public ideaService.model.Ideas toEscapedModel() {
		return new IdeasWrapper(_ideas.toEscapedModel());
	}

	@Override
	public ideaService.model.Ideas toUnescapedModel() {
		return new IdeasWrapper(_ideas.toUnescapedModel());
	}

	@Override
	public int compareTo(ideaService.model.Ideas ideas) {
		return _ideas.compareTo(ideas);
	}

	/**
	* Returns the rating of this ideas.
	*
	* @return the rating of this ideas
	*/
	@Override
	public int getRating() {
		return _ideas.getRating();
	}

	@Override
	public int hashCode() {
		return _ideas.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ideas.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new IdeasWrapper((Ideas)_ideas.clone());
	}

	/**
	* Returns the category of this ideas.
	*
	* @return the category of this ideas
	*/
	@Override
	public java.lang.String getCategory() {
		return _ideas.getCategory();
	}

	/**
	* Returns the description of this ideas.
	*
	* @return the description of this ideas
	*/
	@Override
	public java.lang.String getDescription() {
		return _ideas.getDescription();
	}

	/**
	* Returns the icon of this ideas.
	*
	* @return the icon of this ideas
	*/
	@Override
	public java.lang.String getIcon() {
		return _ideas.getIcon();
	}

	/**
	* Returns the shortdescription of this ideas.
	*
	* @return the shortdescription of this ideas
	*/
	@Override
	public java.lang.String getShortdescription() {
		return _ideas.getShortdescription();
	}

	/**
	* Returns the title of this ideas.
	*
	* @return the title of this ideas
	*/
	@Override
	public java.lang.String getTitle() {
		return _ideas.getTitle();
	}

	/**
	* Returns the user name of this ideas.
	*
	* @return the user name of this ideas
	*/
	@Override
	public java.lang.String getUserName() {
		return _ideas.getUserName();
	}

	/**
	* Returns the user uuid of this ideas.
	*
	* @return the user uuid of this ideas
	*/
	@Override
	public java.lang.String getUserUuid() {
		return _ideas.getUserUuid();
	}

	@Override
	public java.lang.String toString() {
		return _ideas.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ideas.toXmlString();
	}

	/**
	* Returns the create date of this ideas.
	*
	* @return the create date of this ideas
	*/
	@Override
	public Date getCreateDate() {
		return _ideas.getCreateDate();
	}

	/**
	* Returns the modified date of this ideas.
	*
	* @return the modified date of this ideas
	*/
	@Override
	public Date getModifiedDate() {
		return _ideas.getModifiedDate();
	}

	/**
	* Returns the company ID of this ideas.
	*
	* @return the company ID of this ideas
	*/
	@Override
	public long getCompanyId() {
		return _ideas.getCompanyId();
	}

	/**
	* Returns the group ID of this ideas.
	*
	* @return the group ID of this ideas
	*/
	@Override
	public long getGroupId() {
		return _ideas.getGroupId();
	}

	/**
	* Returns the ideas ID of this ideas.
	*
	* @return the ideas ID of this ideas
	*/
	@Override
	public long getIdeasId() {
		return _ideas.getIdeasId();
	}

	/**
	* Returns the primary key of this ideas.
	*
	* @return the primary key of this ideas
	*/
	@Override
	public long getPrimaryKey() {
		return _ideas.getPrimaryKey();
	}

	/**
	* Returns the user ID of this ideas.
	*
	* @return the user ID of this ideas
	*/
	@Override
	public long getUserId() {
		return _ideas.getUserId();
	}

	@Override
	public void persist() {
		_ideas.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ideas.setCachedModel(cachedModel);
	}

	/**
	* Sets the category of this ideas.
	*
	* @param category the category of this ideas
	*/
	@Override
	public void setCategory(java.lang.String category) {
		_ideas.setCategory(category);
	}

	/**
	* Sets the company ID of this ideas.
	*
	* @param companyId the company ID of this ideas
	*/
	@Override
	public void setCompanyId(long companyId) {
		_ideas.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this ideas.
	*
	* @param createDate the create date of this ideas
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_ideas.setCreateDate(createDate);
	}

	/**
	* Sets the description of this ideas.
	*
	* @param description the description of this ideas
	*/
	@Override
	public void setDescription(java.lang.String description) {
		_ideas.setDescription(description);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_ideas.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_ideas.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_ideas.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this ideas.
	*
	* @param groupId the group ID of this ideas
	*/
	@Override
	public void setGroupId(long groupId) {
		_ideas.setGroupId(groupId);
	}

	/**
	* Sets the icon of this ideas.
	*
	* @param icon the icon of this ideas
	*/
	@Override
	public void setIcon(java.lang.String icon) {
		_ideas.setIcon(icon);
	}

	/**
	* Sets the ideas ID of this ideas.
	*
	* @param ideasId the ideas ID of this ideas
	*/
	@Override
	public void setIdeasId(long ideasId) {
		_ideas.setIdeasId(ideasId);
	}

	/**
	* Sets whether this ideas is is visible on map.
	*
	* @param isVisibleOnMap the is visible on map of this ideas
	*/
	@Override
	public void setIsVisibleOnMap(boolean isVisibleOnMap) {
		_ideas.setIsVisibleOnMap(isVisibleOnMap);
	}

	/**
	* Sets the latitude of this ideas.
	*
	* @param latitude the latitude of this ideas
	*/
	@Override
	public void setLatitude(double latitude) {
		_ideas.setLatitude(latitude);
	}

	/**
	* Sets the longitude of this ideas.
	*
	* @param longitude the longitude of this ideas
	*/
	@Override
	public void setLongitude(double longitude) {
		_ideas.setLongitude(longitude);
	}

	/**
	* Sets the modified date of this ideas.
	*
	* @param modifiedDate the modified date of this ideas
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_ideas.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_ideas.setNew(n);
	}

	/**
	* Sets the primary key of this ideas.
	*
	* @param primaryKey the primary key of this ideas
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_ideas.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_ideas.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets whether this ideas is published.
	*
	* @param published the published of this ideas
	*/
	@Override
	public void setPublished(boolean published) {
		_ideas.setPublished(published);
	}

	/**
	* Sets the rating of this ideas.
	*
	* @param rating the rating of this ideas
	*/
	@Override
	public void setRating(int rating) {
		_ideas.setRating(rating);
	}

	/**
	* Sets the shortdescription of this ideas.
	*
	* @param shortdescription the shortdescription of this ideas
	*/
	@Override
	public void setShortdescription(java.lang.String shortdescription) {
		_ideas.setShortdescription(shortdescription);
	}

	/**
	* Sets the title of this ideas.
	*
	* @param title the title of this ideas
	*/
	@Override
	public void setTitle(java.lang.String title) {
		_ideas.setTitle(title);
	}

	/**
	* Sets the user ID of this ideas.
	*
	* @param userId the user ID of this ideas
	*/
	@Override
	public void setUserId(long userId) {
		_ideas.setUserId(userId);
	}

	/**
	* Sets the user name of this ideas.
	*
	* @param userName the user name of this ideas
	*/
	@Override
	public void setUserName(java.lang.String userName) {
		_ideas.setUserName(userName);
	}

	/**
	* Sets the user uuid of this ideas.
	*
	* @param userUuid the user uuid of this ideas
	*/
	@Override
	public void setUserUuid(java.lang.String userUuid) {
		_ideas.setUserUuid(userUuid);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof IdeasWrapper)) {
			return false;
		}

		IdeasWrapper ideasWrapper = (IdeasWrapper)obj;

		if (Objects.equals(_ideas, ideasWrapper._ideas)) {
			return true;
		}

		return false;
	}

	@Override
	public Ideas getWrappedModel() {
		return _ideas;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _ideas.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _ideas.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_ideas.resetOriginalValues();
	}

	private final Ideas _ideas;
}