/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package ideaService.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link ideaService.service.http.IdeasServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see ideaService.service.http.IdeasServiceSoap
 * @generated
 */
@ProviderType
public class IdeasSoap implements Serializable {
	public static IdeasSoap toSoapModel(Ideas model) {
		IdeasSoap soapModel = new IdeasSoap();

		soapModel.setIdeasId(model.getIdeasId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setTitle(model.getTitle());
		soapModel.setDescription(model.getDescription());
		soapModel.setShortdescription(model.getShortdescription());
		soapModel.setCategory(model.getCategory());
		soapModel.setLatitude(model.getLatitude());
		soapModel.setLongitude(model.getLongitude());
		soapModel.setIcon(model.getIcon());
		soapModel.setPublished(model.getPublished());
		soapModel.setIsVisibleOnMap(model.getIsVisibleOnMap());
		soapModel.setRating(model.getRating());

		return soapModel;
	}

	public static IdeasSoap[] toSoapModels(Ideas[] models) {
		IdeasSoap[] soapModels = new IdeasSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static IdeasSoap[][] toSoapModels(Ideas[][] models) {
		IdeasSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new IdeasSoap[models.length][models[0].length];
		}
		else {
			soapModels = new IdeasSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static IdeasSoap[] toSoapModels(List<Ideas> models) {
		List<IdeasSoap> soapModels = new ArrayList<IdeasSoap>(models.size());

		for (Ideas model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new IdeasSoap[soapModels.size()]);
	}

	public IdeasSoap() {
	}

	public long getPrimaryKey() {
		return _ideasId;
	}

	public void setPrimaryKey(long pk) {
		setIdeasId(pk);
	}

	public long getIdeasId() {
		return _ideasId;
	}

	public void setIdeasId(long ideasId) {
		_ideasId = ideasId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		_description = description;
	}

	public String getShortdescription() {
		return _shortdescription;
	}

	public void setShortdescription(String shortdescription) {
		_shortdescription = shortdescription;
	}

	public String getCategory() {
		return _category;
	}

	public void setCategory(String category) {
		_category = category;
	}

	public double getLatitude() {
		return _latitude;
	}

	public void setLatitude(double latitude) {
		_latitude = latitude;
	}

	public double getLongitude() {
		return _longitude;
	}

	public void setLongitude(double longitude) {
		_longitude = longitude;
	}

	public String getIcon() {
		return _icon;
	}

	public void setIcon(String icon) {
		_icon = icon;
	}

	public boolean getPublished() {
		return _published;
	}

	public boolean isPublished() {
		return _published;
	}

	public void setPublished(boolean published) {
		_published = published;
	}

	public boolean getIsVisibleOnMap() {
		return _isVisibleOnMap;
	}

	public boolean isIsVisibleOnMap() {
		return _isVisibleOnMap;
	}

	public void setIsVisibleOnMap(boolean isVisibleOnMap) {
		_isVisibleOnMap = isVisibleOnMap;
	}

	public int getRating() {
		return _rating;
	}

	public void setRating(int rating) {
		_rating = rating;
	}

	private long _ideasId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _groupId;
	private String _title;
	private String _description;
	private String _shortdescription;
	private String _category;
	private double _latitude;
	private double _longitude;
	private String _icon;
	private boolean _published;
	private boolean _isVisibleOnMap;
	private int _rating;
}