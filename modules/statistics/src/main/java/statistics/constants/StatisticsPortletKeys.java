package statistics.constants;

/**
 * @author englmeier
 */
public class StatisticsPortletKeys {

	public static final String Statistics = "Statistics";

}