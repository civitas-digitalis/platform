package mapApi.application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.PortalException;

import ideaService.model.Ideas;
import ideaService.service.IdeasLocalServiceUtil;

/**
 * @author englmeier
 */
@ApplicationPath("/mapService")
@Component(immediate = true, service = Application.class)
public class MapApiApplication extends Application {

	public Set<Object> getSingletons() {
		return Collections.<Object>singleton(this);
	}

	Map<Integer, Marker> markers = new HashMap<Integer,Marker>();

	public MapApiApplication(){
		testInit();
	}

	public void testInit(){
		Marker m1 = new Marker();
		m1.setDescription("desM1");
		m1.setId(1);
		m1.setLatitude(48.174882);
		m1.setLongitude(11.602710);
		m1.setTitle("Nordfriedhof");
		markers.put(m1.getId(), m1);

		Marker m2 = new Marker();
		m2.setDescription("desM2");
		m2.setId(2);
		m2.setLatitude(48.176571);
		m2.setLongitude(11.593055);
		m2.setTitle("Highlight Towers");
		markers.put(m2.getId(), m2);
	}

	@GET
	@Path("/markers/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Ideas getIdeaById(@PathParam("id") String id) throws NumberFormatException, PortalException {
		return IdeasLocalServiceUtil.getIdeas(Integer.parseInt(id));
	}

	//http://localhost:8080/o/mapApi/mapService/markers
	@GET
	@Path("/markers")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Marker> getAllMarkers(){
		List<Ideas> allIdeas = IdeasLocalServiceUtil.getIdeases(0, IdeasLocalServiceUtil.getIdeasesCount());
		List<Marker> markers = new ArrayList<Marker>();
		for(Ideas i : allIdeas){
			markers.add(toMarker(i));
		}
		return markers;
	}

	private Marker toMarker(Ideas i){
		Marker m = new Marker();
		m.setTitle(i.getTitle());
		m.setLatitude(i.getLatitude());
		m.setLongitude(i.getLongitude());
		m.setDescription(i.getDescription());
		return m;
	}
}