<%@ include file="/init.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>
<portlet:defineObjects />
<head>
<script>
    define._amd = define.amd;
    define.amd = false;
</script>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
<script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vex-js/4.0.0/js/vex.combined.min.js"></script>
<script>vex.defaultOptions.className = 'vex-theme-default'</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/vex-js/4.0.0/css/vex.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/vex-js/4.0.0/css/vex-theme-default.css" />
<script>
    define.amd = define._amd;
</script>
</head>
<div id="map"></div>
<style>
<!--
#map {height: 600px;}
-->
</style>

<script type="text/javascript">
$(document).ready(function(){
	//init the map
	var map = L.map('map').setView([48.174918, 11.596160], 13);
	L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	    maxZoom: 18,
	    id: 'mapbox.streets',
	    accessToken: 'your.mapbox.access.token'
	}).addTo(map);
	//load markers from server
	var ideasJsonArray = JSON.parse('${allIdeas}')
	$.each(ideasJsonArray, function (index, value) {
					if(value.isVisibleOnMap){
					var latlng = L.latLng(value.latitude,value.longitude);
			        var loadedMarker = new L.Marker(latlng, {draggable:false});
			        loadedMarker.dbId = value.id;
			        loadedMarker.bindTooltip(value.title).openTooltip();
			        loadedMarker.bindPopup(getMarkerPopUp(value.id,value.title,value.shortdescription));
			        map.addLayer(loadedMarker);
					}
			    });


	function getMarkerPopUp(id, title, description){
		return "<h4>" + title + "</h4>" + "<p>" + description + "</p><br>";
	}
	//adding a marker on click and open a modal
	map.on('click', onMapClick);
		function onMapClick(e) {
			vex.dialog.open({
			    message: 'Enter a new idea:',
			    input: [
			        '<style>',
			            '.vex-custom-field-wrapper {',
			                'margin: 1em 0;',
			            '}',
			            '.vex-custom-field-wrapper > label {',
			                'display: inline-block;',
			                'margin-bottom: .1em;',
			            '}',
			        '</style>',
			        '<div class="vex-custom-field-wrapper">',
			            '<label for="title">Title</label>',
			            '<div class="vex-custom-input-wrapper">',
			                '<input name="title" type="string" placeholder="Title" size="35" />',
			            '</div>',
				        '<div class="vex-custom-field-wrapper">',
			            '<label for="shortdescription">Short Desription</label>',
			            '<div class="vex-custom-input-wrapper">',
			                '<input name="shortdescription" type="string" placeholder="Short Description" size="35" />',
			            '</div>',
				        '<div class="vex-custom-field-wrapper">',
			            '<label for="description">Desription</label>',
			            '<div class="vex-custom-input-wrapper">',
			                '<input name="description" type="string" placeholder="Description" size="35" />',
			            '</div>',
			    ].join(''),
			    callback: function (data) {
			        if (!data) {
			            return;
			        }
	        		//post to db
			        Liferay.Service(
			        		  '/idea.ideas/insert-new-idea',
			        		  {
			        		    title: data.title,
			        		    category: "mapIdea",
			        		    userId: themeDisplay.getUserId(),
			        		    icon: null,
			        		    shortDescription: data.shortdescription,
			        		    description: data.description,
			        		    latitude: e.latlng.lat ,
	        				    longitude: e.latlng.lng ,
			        		    published: true,
			        		    showOnMap: true,
			        		    groupId: themeDisplay.getScopeGroupId(),
			        		    rating : 6
			        		  },
			        		  function(obj) {
			      					 var response = jQuery.parseJSON(obj)
		        					 if(response.created){
	        					        //add new marker to map
		       							var marker = new L.Marker(e.latlng, {draggable:false});
		       					        marker.bindPopup("<h4>" + data.title + "</h4>" + "<p>" + data.shortdescription + "</p>");
		       					        marker.bindTooltip(data.title).openTooltip();
		       					        marker.dbId = response.createdId;
		       			        		map.addLayer(marker);
		       			        		vex.dialog.alert({
		       			        		    message: 'New Idea successfully created',
		       			        		    callback: function (value) {
		       			        		    	Liferay.fire('refreshIdeaBoard', {refresh :true});
		       			        		    }
		       			        		})
		        				  	}else{
		        				  		vex.dialog.alert(response.errorMsg);
		        				  	}
			        		  }
			        		);
			    }
			});

	};
	});

/*
Rest API example
$.getJSON(serverPrefix + "o/mapApi/mapService/markers", function(data){
$.each(data, function (index, value) {
	var latlng = L.latLng(value.latitude,value.longitude);
    var loadedMarker = new L.Marker(latlng, {draggable:false});
    //loadedMarker.bindTooltip(value.title).openTooltip();
    loadedMarker.bindPopup("<h4>" + value.title + "</h4>" + "<p>" + value.description + "</p>");
    map.addLayer(loadedMarker);
});
}); */

//Liferay get Service example
/* 		Liferay.Service(
			  '/idea.ideas/get-all-ideas',
			  function(data) {
				var received = jQuery.parseJSON(data);
				$.each(received, function (index, value) {
					if(value.showOnMap){
					var latlng = L.latLng(value.latitude,value.longitude);
			        var loadedMarker = new L.Marker(latlng, {draggable:false});
			        loadedMarker.dbId = value.id;
			        loadedMarker.bindTooltip(value.title).openTooltip();
			        loadedMarker.bindPopup(getMarkerPopUp(value.id,value.title,value.description));
			        map.addLayer(loadedMarker);
					}
			    });
				}
			); */

</script>