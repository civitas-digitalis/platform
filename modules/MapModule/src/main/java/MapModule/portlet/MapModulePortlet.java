package MapModule.portlet;

import MapModule.constants.MapModulePortletKeys;
import ideaService.service.IdeasLocalServiceUtil;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author englmeier
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.EEN",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=MapModule Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + MapModulePortletKeys.MapModule,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class MapModulePortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException{
		renderRequest.setAttribute("allIdeas", JSONFactoryUtil.looseSerializeDeep(IdeasLocalServiceUtil.getAllIdeas()));
		super.render(renderRequest, renderResponse);
	}

}