package ideaBoard.portlet;

import ideaBoard.constants.IdeaBoardPortletKeys;
import ideaService.model.Comment;
import ideaService.model.Ideas;
import ideaService.service.CommentLocalServiceUtil;
import ideaService.service.IdeasLocalServiceUtil;
import ideaService.service.IdeasService;
import ideaService.service.persistence.IdeasUtil;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author englmeier
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=ideaBoard Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + IdeaBoardPortletKeys.IdeaBoard,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class IdeaBoardPortlet extends MVCPortlet {

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException{
		//System.out.println("Loaded the IdeaBoard.");
		super.doView(renderRequest, renderResponse);
	}

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException{
		renderRequest.setAttribute("allIdeas", JSONFactoryUtil.looseSerializeDeep(IdeasLocalServiceUtil.getAllIdeas()));
		super.render(renderRequest, renderResponse);
	}

	public void getNavigationToInputForm(ActionRequest request, ActionResponse response)
			throws PortalException, SystemException {
        response.setRenderParameter("mvcPath","/inputIdea.jsp");
	}

	public void insertNewIdea(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException, SystemException, IOException {
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
		        Ideas.class.getName(), actionRequest);
		String title = ParamUtil.getString(actionRequest, "title");
		String description = ParamUtil.getString(actionRequest, "description");
		String shortDescription = ParamUtil.getString(actionRequest, "short description");
		Ideas nextIdea = IdeasLocalServiceUtil.creadeIdeasWithAutomatedDbId(title, serviceContext.getUserId(),serviceContext.getScopeGroupId(),
				null, null, shortDescription, description, 0.0, 0.0, true, false,0);
		IdeasLocalServiceUtil.persistIdeasAndPerformTypeChecks(nextIdea);
		//Redirect to avoid duplicate submissions
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		actionResponse.sendRedirect(PortalUtil.getLayoutURL(themeDisplay.getLayout(),themeDisplay));
	}

	public void insertNewComment(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException, IOException {
		ServiceContext serviceContext = ServiceContextFactory.getInstance(
		        Comment.class.getName(), actionRequest);
		String commentBody = ParamUtil.getString(actionRequest, "Comment");
		String id = ParamUtil.getString(actionRequest, "IdeaId");
		Comment nextComment = CommentLocalServiceUtil.createCommentWithAutomatedDbId(id,commentBody,serviceContext.getUserId());
		CommentLocalServiceUtil.persistCommentAndPerformTypeChecks(nextComment);
		//Redirect to avoid duplicate submissions
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		actionResponse.sendRedirect(PortalUtil.getLayoutURL(themeDisplay.getLayout(),themeDisplay));
	}
}