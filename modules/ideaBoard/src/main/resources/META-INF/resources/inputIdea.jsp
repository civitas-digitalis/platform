<%@ include file="/init.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %>
<portlet:defineObjects />
<portlet:actionURL  var="submitNewIdea" windowState="normal" name="insertNewIdea"></portlet:actionURL>

<aui:form action="<%=submitNewIdea%>" method="post" name="myForm" id="myForm">
<aui:input type="text" name="title"/>
<aui:input type="text" name="short description"/>
<aui:input type="text" name="description"/>
<aui:button type="button" class = "btn btn-primary" id="next" value="done" onClick="submitForm()"/>
</aui:form>

<script>
function submitForm(e){
	var myForm = $('#<portlet:namespace/>myForm');
	myForm.submit();
}
</script>