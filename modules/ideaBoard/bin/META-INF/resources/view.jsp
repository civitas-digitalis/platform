<%@ include file="/init.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui" %>
<%@ page import="com.liferay.portal.kernel.util.PortalUtil"%>
<portlet:defineObjects />

<portlet:actionURL  var="navigateToInputForm" windowState="normal" name="getNavigationToInputForm">
</portlet:actionURL>

<style>
#bottomHr {
    width: 100%;
    height: 1px;
    margin: 5px;
    color: blue;
    background: black;
}
#ideas_tab{
	width : 85%;
}
.yui3-widget.tab{
	background-color:rgba(0, 0, 0, 0.25);
	margin-left : -5px;
}
.yui3-widget.tab.active.tab-selected{
	background-color:rgba(204, 0, 0, 0.5);
	transform: scale(1.1);
}
.tab-pane.active{
	transform : scale (0.1);
}
.tab-content{
	width : 75%;
	display: inline-grid;
}
.nav.nav-stacked{
	float:left;
	display: inline-grid;
}
.nav{
	padding-left:0;
	padding-right:5%;
	margin-bottom: 20px;
}
.nav-stacked > li + li{
	margin-top: 4px;
}
</style>

<div id="main">
<aui:button type="button"class = "btn btn-primary" id="newBtn" style="float:right;" value="New Idea" onClick="<%=navigateToInputForm.toString()%>"/>
<script type="text/javascript">
$(document).ready(function() {
	 Liferay.on('refreshIdeaBoard',
	 function(event) {
	 var refresh = event.refresh;
		if(refresh){
			location.reload(true);
		}
	 });
});

var ideasJsonArray = JSON.parse('${allIdeas}')
var tabViewChildren = ideasArrayToTabViewChildren(ideasJsonArray);
YUI().use('aui-tabview', function(Y){
	new Y.TabView(
			{
		children : tabViewChildren,
		srcNode : '#ideas_tab',
		stacked : true
	}).render();
});

function ideasArrayToTabViewChildren(arr){
	var result = new Array();
	for (var i = 0; i < arr.length; i++){
		var obj = arr[i];
		var child = {
					content: '<br><h3>' + obj.shortdescription + '</h3><br><p>' + obj.description + '</p>',
					id : obj.id,
					label: ''+obj.title
				}
		result.push(child);
	}
	return result;
}

</script>
<div id="ideas_tab"></div>
<br/><hr id="bottomHr"/><br/>

<portlet:actionURL  var="submitNewComment" windowState="normal" name="insertNewComment"></portlet:actionURL>

<aui:form action="<%=submitNewComment%>" method="post" name="commentForm" id="commentForm">
<aui:input type="text" name="Comment"></aui:input>
<aui:input type="hidden" name="IdeaId" value="0"/>
<aui:button type="button" class = "btn btn-primary" id="next" value="Submit" onClick="submitForm()"/>
</aui:form>

<script>
function submitForm(e){
	var myForm = $('#<portlet:namespace/>commentForm');
	var IdOfSelectedIdea = $('.yui3-widget.tab.active.tab-selected').attr('id');
	document.getElementById('_IdeaBoard_INSTANCE_J6vRLVcyJ4mj_IdeaId').value = IdOfSelectedIdea;
	myForm.submit();
}
</script>
</div>

